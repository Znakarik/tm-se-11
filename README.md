# Developer info
Didenko Olga - znakarik@icloud.com

# Build Maven project
```
mvn clean install 
```

# Run taskmanager in terminal 
```
java -jar taskmanager-1.0.0.jar
```

# System requirement
 
* Java - openjdk version "13.0.2"

* Mac OS Mojave 10.14.5

# Functional
* Registry account & delete account
* Login & logout
* Saving & uploading user, projects & session data in formats: json, xml, binary
* Password hashing using MD5 hash-function
* Project, task & user CRUD operations 

# Instruments
- JAX-WS
- JAXB
- Reflection API
- FasterXML
- Lombok and more .. or not :)
