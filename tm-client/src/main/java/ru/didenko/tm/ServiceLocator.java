package ru.didenko.tm;

import ru.didenko.tm.endpoint.ProjectEndpoint;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.endpoint.TaskEndpoint;
import ru.didenko.tm.endpoint.UserEndpoint;
import ru.didenko.tm.service.TerminalService;

public interface ServiceLocator {

    TerminalService getTerminalService();

    ProjectEndpoint getProjectEndpoint();

    TaskEndpoint getTaskEndpoint();

    UserEndpoint getUserEndpoint();

    Session getSession();

    void setSession(Session session);
}
