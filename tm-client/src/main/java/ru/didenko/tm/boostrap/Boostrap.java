package ru.didenko.tm.boostrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.didenko.tm.ServiceLocator;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.*;
import ru.didenko.tm.service.TerminalService;

import java.lang.Exception;
import java.util.*;

public class Boostrap implements ServiceLocator {

    @NotNull
    private final TerminalService terminalService = new TerminalService();

    @NotNull
    private Session session;

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    @NotNull
    final Map<String, AbstractCommand<? extends AbstractCommand<?>>> commandMap = new LinkedHashMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes1 = new Reflections("ru.didenko.tm").getSubTypesOf(ru.didenko.tm.command.AbstractCommand.class);

    public Map<String, AbstractCommand<? extends AbstractCommand<?>>> getCommandMap() {
        return commandMap;
    }

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public Session getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

    public final void registryOne(final @NotNull AbstractCommand command) {
        if (command == null) return;
        command.setBoostrap(this);
        commandMap.put(command.getName(), command);
    }

    public final void registryAll() throws IllegalAccessException, InstantiationException {
        @NotNull final List<Class<? extends AbstractCommand>> comandList = new LinkedList<>(classes1);
        for (Class<? extends AbstractCommand> command : comandList) {
            if (AbstractCommand.class.isAssignableFrom(command)) {
                AbstractCommand<?> abstractCommand = command.newInstance();
                registryOne(abstractCommand);
            }
        }
    }

    public final void execute(final @NotNull String command) throws Exception {
        if (command == null) return;
        @NotNull final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand.isSecure()) {
            if (getSession().getRole().value().equals(Role.ADMIN.value())) {
                try {
                    abstractCommand.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else System.err.println("YOU DON'T HAVE PERMISSION");
        } else if (!abstractCommand.isSecure()) {
            try {
                abstractCommand.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void before() throws Exception {
        registryAll();
    }


    public final void init() {
        @NotNull final TerminalService reader = getTerminalService();
        String inputCommand = "";
        System.out.println("Connecting to server...");
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        try {
            before();
        } catch (Exception e) {
            System.err.println("SMTHG IN INITIALIZING");
        }
        do {
            try {
                if (getSession() == null) {
                    inputCommand = "user-login";
                } else if (getUserEndpoint().findAllUser(getSession()).size() > 0) {
//                    if (this.getUserEndpoint().getCurrentUser(getSession()) != null) {
                        inputCommand = reader.nextLine();
//                    }
                    // Здесь может быть NPE, так как если сессии нет, но список не получится
                } else if (getUserEndpoint().findAllUser(getSession()).size() == 0) {
                    inputCommand = "user-register";
                }
                execute(inputCommand);
            } catch (Exception e) {
                System.err.println("EXCEPTION");
                e.printStackTrace();
            }
        } while (!"exit".equals(inputCommand));
    }
}
