package ru.didenko.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.boostrap.Boostrap;
import ru.didenko.tm.service.TerminalService;

import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public abstract class AbstractCommand<T extends AbstractCommand<T>> {

    @NotNull
    protected Boostrap boostrap;
    @NotNull
    protected TerminalService terminalService;

    public void setBoostrap(@NotNull Boostrap boostrap) {
        this.boostrap = boostrap;
        this.terminalService = boostrap.getTerminalService();
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean isSecure();

}
