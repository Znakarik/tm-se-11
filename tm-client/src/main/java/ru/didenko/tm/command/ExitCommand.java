package ru.didenko.tm.command;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand<ExitCommand> {

    @Override
    public final String getName() {
        return "exit";
    }

    @Override
    public final String getDescription() {
        return "Exit task manager";
    }

    @Override
    public final void execute() throws Exception {
        System.exit(0);
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
