package ru.didenko.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.endpoint.Project;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.endpoint.Task;

import java.security.NoSuchAlgorithmException;

public class SearchCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "search";
    }

    @Override
    public String getDescription() {
        return "Search project or task";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session currentSession = boostrap.getSession();
        System.out.println("[SEARCH]");
        System.out.println("ENTER NAME OR PART OF DESCRIPTION");
        String searchChoice = boostrap.getTerminalService().nextLine();
        if (searchChoice == null) return;
        Project project = boostrap.getProjectEndpoint().searchProject(currentSession, searchChoice);
        if (project != null) {
            System.out.println(project.toString());
        } else if (project == null) {
            Task task = boostrap.getTaskEndpoint().searchTask(currentSession, searchChoice);
            if (task != null) System.out.println(task.toString());
            else if (task == null) {
                System.err.println("THERE'S NO PROJECT OR TASK");
            }
        }
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
