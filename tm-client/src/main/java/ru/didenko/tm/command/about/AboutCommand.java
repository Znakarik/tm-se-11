package ru.didenko.tm.command.about;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;

import java.io.File;
import java.io.FileInputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

@NoArgsConstructor
public class AboutCommand extends AbstractCommand<AboutCommand> {

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Information about application";
    }

    @Override
    public void execute() throws Exception, NoSuchAlgorithmException {
        final File file = new File("buildNumber.properties");
        final Properties properties = new Properties();

        final String javaVersion = Manifests.read("Originally-Created-By");
        properties.load(new FileInputStream(file));
        final String buildNumber = properties.getProperty("buildNumber");
        System.out.format("***\nBUILD NUMBER: %s\nJAVA VERSION: %s\n***", buildNumber, javaVersion);
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
