package ru.didenko.tm.command.data.store;

import ru.didenko.tm.command.AbstractCommand;

public class DataExportBinCommand extends AbstractCommand<DataExportBinCommand> {

    @Override
    public String getName() {
        return "save-bin";
    }

    @Override
    public String getDescription() {
        return "Save data in binary format";
    }

    @Override
    public void execute() throws Exception {
        boostrap.getUserEndpoint().binSaveUser(boostrap.getSession());
        System.out.println("[DATA SAVED]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
