package ru.didenko.tm.command.data.store;

import ru.didenko.tm.command.AbstractCommand;

public class DataExportJsonJacksonCommand extends AbstractCommand<DataExportJsonJacksonCommand> {
    @Override
    public String getName() {
        return "save-json-jackson";
    }

    @Override
    public String getDescription() {
        return "Save data in JSON using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        boostrap.getUserEndpoint().jsonJacksonSaveUser(boostrap.getSession());
        System.out.println("[DATA SAVED]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
