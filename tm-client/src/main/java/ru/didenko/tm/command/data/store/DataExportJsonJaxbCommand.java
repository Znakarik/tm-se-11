package ru.didenko.tm.command.data.store;

import ru.didenko.tm.command.AbstractCommand;

public class DataExportJsonJaxbCommand extends AbstractCommand<DataExportJsonJaxbCommand> {
    @Override
    public String getName() {
        return "save-jaxb-json";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void execute() throws Exception {
        boostrap.getUserEndpoint().jsonJaxbSaveUser(boostrap.getSession());
        System.out.println("[DATA SAVED]");

    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
