package ru.didenko.tm.command.data.store;

import ru.didenko.tm.command.AbstractCommand;

public class DataExportXmlJacksonCommand extends AbstractCommand<DataExportXmlJacksonCommand> {
    @Override
    public String getName() {
        return "save-xml-jackson";
    }

    @Override
    public String getDescription() {
        return "Save data in XML using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        boostrap.getUserEndpoint().xmlJacksonSaveUser(boostrap.getSession());
        System.out.println("[DATA SAVED]");


    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
