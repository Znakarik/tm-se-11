package ru.didenko.tm.command.data.store;

import ru.didenko.tm.command.AbstractCommand;

public class DataExportXmlJaxbCommand extends AbstractCommand<DataExportXmlJaxbCommand> {
    @Override
    public String getName() {
        return "save-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Save in XML format using JAXB";
    }

    @Override
    public void execute() throws Exception {
        boostrap.getUserEndpoint().xmlJaxbSaveUser(boostrap.getSession());
        System.out.println("[DATA SAVED]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
