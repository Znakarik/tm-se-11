package ru.didenko.tm.command.data.upload;

import ru.didenko.tm.command.AbstractCommand;

public class DataImportDataBinCommand extends AbstractCommand<DataImportDataBinCommand> {
    @Override
    public String getName() {
        return "load-data-bin";
    }

    @Override
    public String getDescription() {
        return "Load data from binary";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOADING DATA...]");
        boostrap.getUserEndpoint().binLoadUser(boostrap.getSession());
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
