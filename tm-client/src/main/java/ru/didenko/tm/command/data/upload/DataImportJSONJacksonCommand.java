package ru.didenko.tm.command.data.upload;

import ru.didenko.tm.command.AbstractCommand;

public class DataImportJSONJacksonCommand extends AbstractCommand<DataImportJSONJacksonCommand> {
    @Override
    public String getName() {
        return "load-json-jackson";
    }

    @Override
    public String getDescription() {
        return "Load data from JSON using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOADING DATA...]");
        boostrap.getUserEndpoint().jsonJacksonLoadUser(boostrap.getSession());
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
