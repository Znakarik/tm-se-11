package ru.didenko.tm.command.data.upload;

import ru.didenko.tm.command.AbstractCommand;

public class DataImportXMLJacksonCommand extends AbstractCommand<DataImportXMLJacksonCommand> {
    @Override
    public String getName() {
        return "load-xml-jackson";
    }

    @Override
    public String getDescription() {
        return "Load data from XML using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOADING DATA...]");
        boostrap.getUserEndpoint().xmlJacksonLoadUser(boostrap.getSession());
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
