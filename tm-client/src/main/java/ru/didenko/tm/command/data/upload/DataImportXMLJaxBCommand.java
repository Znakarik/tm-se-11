package ru.didenko.tm.command.data.upload;

import ru.didenko.tm.command.AbstractCommand;

public class DataImportXMLJaxBCommand extends AbstractCommand<DataImportXMLJaxBCommand> {
    @Override
    public String getName() {
        return "load-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Load data from XML using JAXB";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOADING DATA...]");
        boostrap.getUserEndpoint().xmlJaxbLoadUser();
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
