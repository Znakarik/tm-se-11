package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.*;

import java.lang.Exception;
import java.util.Iterator;

@NoArgsConstructor
public final class ProjectDeleteCommand extends AbstractCommand<ProjectDeleteCommand> {

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER NAME");
        @NotNull final Session currentSession = boostrap.getSession();
        User currentUser = boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName());
        final String projectName = boostrap.getTerminalService().nextLine();
        if (currentUser.getRole() == Role.ADMIN) {
            Project project = boostrap.getProjectEndpoint().findOneByNameProject(currentSession, projectName);
            if (boostrap.getTaskEndpoint().findAllTask(boostrap.getSession()).size() != 0) {
                boostrap.getTaskEndpoint().findAllTask(boostrap.getSession()).removeIf(task -> task.getProjectId().equals(project.getId()));
                boostrap.getProjectEndpoint().deleteProject(boostrap.getSession(), projectName);
            } else {
                boostrap.getProjectEndpoint().deleteProject(boostrap.getSession(), projectName);
            }
        } else {
            // Удаление проекта и подтасков для юзера (юзер может удалить только свои проекты)
            String currentUserId = currentUser.getId();

            Project project = boostrap.getProjectEndpoint().secureGetOneProject(currentSession, currentUserId, projectName);
            Iterator<Project> projectIterator = boostrap.getProjectEndpoint().getSecureListProject(currentSession, currentUserId).iterator();
            Iterator<Task> taskIterator = boostrap.getTaskEndpoint().getSecureListTask(currentSession, currentUserId).iterator();

            while (projectIterator.hasNext()) {
                Project projectFromRepo = projectIterator.next();
                if (project.getUserId() != null) {
                    if (boostrap.getTaskEndpoint().findAllTask(currentSession).size() != 0) {
                        Task task = taskIterator.next();
                        if (task.getProjectId() != null && task.getProjectId().equals(projectFromRepo.getId())) {
                            taskIterator.remove();
                        }
                    }
                    projectIterator.remove();
                }
            }
        }
        System.out.println("[PROJECT DELETED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
