package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.*;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand<ProjectListCommand> {

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session currentSession = boostrap.getSession();
        @Nullable User currentUser = boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName());
        System.out.println("[PROJECT LIST]");
        @NotNull int index = 1;
        System.out.println("SHOW PROJECTS BY: START, FINISH, CREATION, STATUS?");
        final @NotNull String choice = boostrap.getTerminalService().nextLine();
        @Nullable List<Project> projects = new ArrayList<>();

        if (currentUser.getRole() == Role.USER) {
            projects.addAll(boostrap.getProjectEndpoint().getSecureListProject(currentSession, currentUser.getId()));
        } else if (currentUser.getRole() == Role.ADMIN) {
            projects.addAll(boostrap.getProjectEndpoint().getListProject(currentSession));
        }

        if (projects == null) return;
        switch (choice) {
            case "start":
                boostrap.getProjectEndpoint().sortByStartProject(currentSession, projects);
                break;
            case "finish":
                boostrap.getProjectEndpoint().sortByFinishProject(currentSession, projects);
                break;
            case "creation":
                boostrap.getProjectEndpoint().sortByCreationProject(currentSession, projects);
            case "status":
                boostrap.getProjectEndpoint().sortByStatusProject(currentSession, projects);
                break;
            default:
                System.err.println("TRY AGAIN");
                execute();
                break;
        }

        if (projects == null) return;
        for (@NotNull Project project : projects) {
            System.out.println(index++ + ". " + project.getName());
        }

    }


    @Override
    public boolean isSecure() {
        return false;
    }
}
