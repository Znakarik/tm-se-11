package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.*;

import java.lang.Exception;
import java.util.Iterator;

@NoArgsConstructor
public final class ProjectRemoveAllCommand extends AbstractCommand<ProjectRemoveAllCommand> {

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        @Nullable final Session currentSession = boostrap.getSession();
        @Nullable User currentUser = boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName());
        Iterator<Task> taskIterator = boostrap.getTaskEndpoint().findAllTask(currentSession).iterator();
        Iterator<Project> projectIterator = boostrap.getProjectEndpoint().findAllProject(currentSession).iterator();
        while (projectIterator.hasNext()) {
            Project project = projectIterator.next();
            if (currentUser.getRole() == Role.ADMIN) {
                while (taskIterator.hasNext()) {
                    Task task = taskIterator.next();
                    // Логика под админа - может удалять все проекты
                    if (project.getId().equals(task.getProjectId())) {
                        taskIterator.remove();
                    }
                }
                projectIterator.remove();
            }

            // Юзер может удалять только свои проекты
            if ((currentUser.getRole() == Role.USER)) {
                if (boostrap.getTaskEndpoint().findAllTask(currentSession).size() > 0) {
                    while (taskIterator.hasNext()) {
                        if (project.getId().equals(taskIterator.next().getProjectId()) && project.getUserId().equals(currentUser.getId())) {
                            taskIterator.remove();
                        }
                    }
                }
                if (project.getUserId().equals(currentUser.getId())) {
                    projectIterator.remove();
                }
            }
        }
        System.out.println("[PROJECTS DELETED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
