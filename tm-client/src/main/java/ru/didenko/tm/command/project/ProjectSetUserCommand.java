package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.service.TerminalService;

import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class ProjectSetUserCommand extends AbstractCommand<ProjectSetUserCommand> {

    @Override
    public String getName() {
        return "project-set-user";
    }

    @Override
    public String getDescription() {
        return "Set user to project";
    }

    @Override
    public void execute() throws Exception, NoSuchAlgorithmException {
        @Nullable final Session currentSession = boostrap.getSession();
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        System.out.println("[SET USER TO PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        final String projectName = reader.nextLine();
        System.out.println("ENTER USER LOGIN");
        final String userName = reader.nextLine();
        boostrap.getProjectEndpoint().findOneByNameProject(currentSession, projectName)
                .setUserId(boostrap.getUserEndpoint().findOneByNameUser(currentSession, userName).getId());
        System.out.println("[USER SET TO PROJECT]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
