package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand<TaskClearCommand> {

    @Override
    public final String getName() {
        return "task-clear";
    }

    @Override
    public final String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[TASK CLEAR]");
        boostrap.getTaskEndpoint().removeAllTask(boostrap.getSession());
        System.out.println("[ALL TASK DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
