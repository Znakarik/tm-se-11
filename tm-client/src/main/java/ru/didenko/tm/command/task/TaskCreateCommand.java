package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Role;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.endpoint.User;
import ru.didenko.tm.service.TerminalService;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand<TaskCreateCommand> {

    @Override
    public final String getName() {
        return "task-create";
    }

    @Override
    public final String getDescription() {
        return "Create new task";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session currentSession = boostrap.getSession();
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = reader.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = reader.nextLine();
        System.out.println("ENTER START DATE IN FORMAT : DD.MM.YYYY");
        final String startDate = reader.nextLine();
        System.out.println("ENTER FINISH DATE IN FORMAT : DD.MM.YYYY");
        final String finishDate = reader.nextLine();
        boostrap.getTaskEndpoint().createNewTask(currentSession, name, description, startDate, finishDate);
        @Nullable User current = boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName());
        if (current.getRole() == Role.USER) {
            boostrap.getTaskEndpoint().findOneByNameTask(currentSession, name).setUserId(current.getId());
        }
        if (current.getRole() == Role.ADMIN) {
            System.out.println("HELLO ADMIN. SET TASK TO YOUR PROFILE? y/n");
            final String answer = reader.nextLine();
            if (answer.equals("y")) {
                boostrap.getTaskEndpoint().findOneByNameTask(currentSession, name).setUserId(current.getId());
            }
        }

        System.out.println("[TASK CREATED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
