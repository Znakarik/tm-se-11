package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.*;
import ru.didenko.tm.service.TerminalService;

import java.lang.Exception;
import java.util.Iterator;

@NoArgsConstructor
public final class TaskDeleteCommand extends AbstractCommand<TaskDeleteCommand> {

    @Override
    public final String getName() {
        return "task-remove";
    }

    @Override
    public final String getDescription() {
        return "Remove selected task";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session currentSession = boostrap.getSession();
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        System.out.println("ENTER PROJECT NAME");
        final String projectName = reader.nextLine();
        System.out.println("ENTER TASK NAME");
        final String taskName = reader.nextLine();
        final Iterator<Project> iterator = boostrap.getProjectEndpoint().findAllProject(currentSession).iterator();
        final Iterator<Task> taskIterator = boostrap.getTaskEndpoint().findAllTask(currentSession).iterator();
        @Nullable User current = boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName());
        while (iterator.hasNext()) {
            final String p = iterator.next().getName();
            if (p.equals(projectName)) {
                while (taskIterator.hasNext()) {
                    final Task task = taskIterator.next();
                    if (current.getRole() == Role.ADMIN) {
                        if (task.getName().equals(taskName)) {
                            taskIterator.remove();
                        }
                    }
                    if (current.getRole() == Role.USER) {
                        if (task.getUserId().equals(current.getId())) {
                            taskIterator.remove();
                        }
                    }
                }
            }
        }
        System.out.println("[TASK DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}