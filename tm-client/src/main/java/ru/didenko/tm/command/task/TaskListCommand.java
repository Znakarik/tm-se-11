package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.*;

import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand<TaskListCommand> {

    @Override
    public final String getName() {
        return "task-list";
    }

    @Override
    public final String getDescription() {
        return "Show all tasks";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session currentSession = boostrap.getSession();
        System.out.println("[TASK LIST]");
        @NotNull int index = 1;
        System.out.println("SHOW TASKS BY: START, FINISH, CREATION, STATUS?");
        final @NotNull String choice = terminalService.nextLine();
        List<Task> tasks = null;
        User currentUser = boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName());

        if (currentUser.getRole() == Role.USER) {
            tasks = boostrap.getTaskEndpoint().getSecureListTask(boostrap.getSession(), currentUser.getId());
        } else if (currentUser.getRole() == Role.ADMIN) tasks = boostrap.getTaskEndpoint().getListTask(currentSession);

        assert tasks != null;
        switch (choice) {
            case "start":
                boostrap.getTaskEndpoint().sortByStartTask(currentSession, tasks);
                break;
            case "finish":
                boostrap.getTaskEndpoint().sortByFinishTask(currentSession, tasks);
                break;
            case "creation":
                boostrap.getTaskEndpoint().sortByCreationTask(currentSession, tasks);
                break;
            case "status":
                boostrap.getTaskEndpoint().sortByStatusTask(currentSession, tasks);
                break;
            default:
                System.err.println("REPEAT PLZ");
                execute();
                break;
        }
        if (tasks == null) return;
        for (@NotNull Task task : tasks) {
            System.out.println(index++ + ". " + task.getName());
        }
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
