package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.service.TerminalService;

@NoArgsConstructor
public final class TaskSetProject extends AbstractCommand<TaskSetProject> {

    @Override
    public final String getName() {
        return "task-set-project";
    }

    @Override
    public final String getDescription() {
        return "Set project to task";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        System.out.println("[SET PROJECT TO TASK]");
        System.out.println("ENTER TASK NAME");
        final String taskName = reader.nextLine();
        System.out.println("ENTER PROJECT NAME");
        final String projectName = reader.nextLine();
        boostrap.getTaskEndpoint().findOneByNameTask(boostrap.getSession(), taskName).setProjectId(boostrap.getProjectEndpoint()
                .findOneByNameProject(boostrap.getSession(), projectName).getId());
        System.out.println("[PROJECT TO TASK HAS SET]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
