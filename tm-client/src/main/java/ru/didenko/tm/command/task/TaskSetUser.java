package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.service.TerminalService;

@NoArgsConstructor
public final class TaskSetUser extends AbstractCommand<TaskSetUser> {

    @Override
    public final String getName() {
        return "task-set-user";
    }

    @Override
    public final String getDescription() {
        return "Set current user to task";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        @NotNull final Session currentSession = boostrap.getSession();
        System.out.println("[SET TASK TO USER]");
        System.out.println("ENTER TASK NAME");
        final String taskName = reader.nextLine();
        System.out.println("[ENTER USER LOGIN]");
        final String userName = reader.nextLine();
        boostrap.getTaskEndpoint().findOneByNameTask(currentSession, taskName).setUserId(currentSession.getUserId());
        System.out.println("[TASK HAS SET]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
