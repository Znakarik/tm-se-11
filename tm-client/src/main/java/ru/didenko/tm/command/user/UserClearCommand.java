package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.endpoint.User;

import java.util.Iterator;

@NoArgsConstructor
public final class UserClearCommand extends AbstractCommand<UserClearCommand> {

    @Override
    public final String getName() {
        return "user-clear";
    }

    @Override
    public final String getDescription() {
        return "Delete all users";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session currentSession = boostrap.getSession();
        @NotNull final Iterator<User> iterator = boostrap.getUserEndpoint().findAllUser(currentSession).iterator();
        System.out.println("[REMOVE ALL USERS]");
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
        if (boostrap.getUserEndpoint().findAllUser(currentSession).size() == 0) {
            System.out.println("[ALL USERS DELETED]");
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
