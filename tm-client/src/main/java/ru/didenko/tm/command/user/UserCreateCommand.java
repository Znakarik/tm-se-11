package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Role;
import ru.didenko.tm.service.TerminalService;

import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserCreateCommand extends AbstractCommand<UserCreateCommand> {

    @Override
    public final String getName() {
        return "user-create";
    }

    @Override
    public final String getDescription() {
        return "Create new user";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        System.out.println("[CREATE USER]");
        System.out.println("ENTER LOGIN");
        final String login = reader.nextLine();
        System.out.println("ENTER PASSWORD");
        final String pass = reader.nextLine();
        System.out.println("CHOOSE ROLE");
        final String role = reader.nextLine();
        boostrap.getUserEndpoint().createUser(boostrap.getSession(), login, pass, (role.contains("admin"))
                ? Role.ADMIN : Role.USER);
        System.out.println("[USER CREATED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
