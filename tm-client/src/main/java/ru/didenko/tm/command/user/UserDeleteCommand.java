package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Project;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.endpoint.Task;
import ru.didenko.tm.endpoint.User;
import ru.didenko.tm.service.TerminalService;

import java.util.Iterator;

@NoArgsConstructor
public final class UserDeleteCommand extends AbstractCommand<UserDeleteCommand> {

    @Override
    public final String getName() {
        return "user-delete";
    }

    @Override
    public final String getDescription() {
        return "Remove selected user ";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final Session currentSession = boostrap.getSession();
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        System.out.println("[DELETE USER]");
        System.out.println("ENTER LOGIN");
        final String login = reader.nextLine();
        final Iterator<Project> projectIterator = boostrap.getProjectEndpoint().findAllProject(currentSession).iterator();
        final Iterator<Task> taskIterator = boostrap.getTaskEndpoint()
                .findAllTask(currentSession).iterator();
        // Если есть привязанные проект к юзеру - удаляем
        while (projectIterator.hasNext()) {
            if (projectIterator.next().getUserId().equals(boostrap.getSession().getUserId())) {
                projectIterator.remove();
            }
        }
        // Если есть привязанные таски к юзеру - удаляем
        while (taskIterator.hasNext()) {
            if (taskIterator.next().getUserId().equals(boostrap.getSession().getUserId())) {
                taskIterator.remove();
            }
            if (taskIterator.hasNext()) taskIterator.next();
        }
        // Создаем условия для разлога, если удаляем себя
        boostrap.getUserEndpoint().deleteUser(currentSession, login);
        if (login.equals(boostrap.getSession().
                getUserName())) {
            boostrap.setSession(null);
        }
        System.out.println("[USER DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}