package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.service.TerminalService;

import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserEditLoginCommand extends AbstractCommand<UserEditLoginCommand> {

    @Override
    public final String getName() {
        return "user-edit-name";
    }

    @Override
    public final String getDescription() {
        return "Edit user name";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        @NotNull final Session currentSession = boostrap.getSession();
        System.out.println("[EDIT USER LOGIN]");
        System.out.println("ENTER NEW LOGIN");
        final String newUserName = reader.nextLine();
        if (newUserName != null) return;
        boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName()).setName(newUserName);
        System.out.println("[LOGIN CHANGED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
