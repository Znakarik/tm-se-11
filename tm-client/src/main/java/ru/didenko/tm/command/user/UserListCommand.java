package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.User;

@NoArgsConstructor
public final class UserListCommand extends AbstractCommand<UserListCommand> {

    @Override
    public final String getName() {
        return "user-list";
    }

    @Override
    public final String getDescription() {
        return "Show all users";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[USER LIST]");
        int index = 1;
        for (User user : boostrap.getUserEndpoint().findAllUser(boostrap.getSession())) {
            System.out.println(index++ + ". " + user.getName());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
