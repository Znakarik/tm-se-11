package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.*;
import ru.didenko.tm.service.TerminalService;
import ru.didenko.tm.util.HasherPassword;

import java.lang.Exception;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserLoginCommand extends AbstractCommand<UserLoginCommand> {

    @Override
    public final String getName() {
        return "user-login";
    }

    @Override
    public final String getDescription() {
        return "Log in task manager";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        @NotNull final Session currentSession = boostrap.getSession();
        @Nullable Session session = boostrap.getSession();
        System.out.println("[LOGGING IN]");
        System.out.println("ARE YOU A NEW USER? ENTER yes/no");
        switch (reader.nextLine()) {
            case "no":
                System.out.println("ENTER LOGIN");
                final String login = reader.nextLine();
                System.out.println("ENTER PASSWORD");
                final String pass = reader.nextLine();
                session = boostrap.getSessionEndpoint().createNewSession(login, pass);
                boostrap.setSession(session);
                while (session == null) {
                    System.err.println("INVALID LOGIN OR PASS");
                    execute();
                }
                break;
            case "yes":
                System.out.println("[REGISTER NEW USER]");
                System.out.println("ENTER LOGIN");
                final String login1 = reader.nextLine();
                System.out.println("ENTER PASSWORD");
                final String pass1 = reader.nextLine();
                System.out.println("CHOOSE ROLE");
                final String role = reader.nextLine();
                boostrap.getUserEndpoint().createUser(currentSession, login1, pass1, (role.contains("admin")) ? Role.ADMIN : Role.USER);
                System.out.println("[NEW USER REGISTERED]");
                break;
        }
        if (session != null) {
            System.out.println("[YOU'RE LOGGED]");

        }
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
