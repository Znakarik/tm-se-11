package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand<UserLogoutCommand> {

    @Override
    public final String getName() {
        return "user-logout";
    }

    @Override
    public final String getDescription() {
        return "Log out from task manager";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOG OUT FROM TASK MANAGER]");
        boostrap.setSession(null);
        System.out.println("YOU'RE LOGGED OUT");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
