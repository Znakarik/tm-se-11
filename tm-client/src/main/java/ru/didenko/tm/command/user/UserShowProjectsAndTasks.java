package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Exception_Exception;
import ru.didenko.tm.endpoint.Session;

@NoArgsConstructor
public class UserShowProjectsAndTasks extends AbstractCommand<UserShowProjectsAndTasks> {

    @Override
    public final String getName() {
        return "user-projects&tasks";
    }

    @Override
    public final String getDescription() {
        return "Show my projects and tasks";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session currentSesion = boostrap.getSession();
        boostrap.getProjectEndpoint().findAllProject(currentSesion).forEach(project -> {
            try {
                if (project.getUserId().equals(boostrap.getSession().getUserId())) {
                    System.out.println(project.toString());
                    boostrap.getTaskEndpoint().findAllTask(currentSesion).forEach(task -> {
                        if (task.getProjectId().equals(project.getId())) {
                            System.out.println(task.toString());
                        }
                    });
                } else System.err.println("THERE'RE NO YOUR'S PROJECT");
            } catch (Exception_Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
