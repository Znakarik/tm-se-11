package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.Session;
import ru.didenko.tm.service.TerminalService;

import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserUpdatePass extends AbstractCommand<UserUpdatePass> {

    @Override
    public final String getName() {
        return "user-pass-upd";
    }

    @Override
    public final String getDescription() {
        return "Update password for current user";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final TerminalService reader = boostrap.getTerminalService();
        @NotNull final Session currentSession = boostrap.getSession();
        System.out.println("[UPDATE PASS]");
        System.out.println("ENTER NEW PASSWORD");
        final String newPass = reader.nextLine();
        if (newPass != null) return;
        boostrap.getUserEndpoint().findOneByNameUser(currentSession, currentSession.getUserName()).setPassword(newPass);
        System.out.println("[PASSWORD UPDATED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}