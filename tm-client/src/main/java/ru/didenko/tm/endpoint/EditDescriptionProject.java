
package ru.didenko.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for editDescriptionProject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="editDescriptionProject"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arg0" type="{http://endpoint.tm.didenko.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="entity" type="{http://endpoint.tm.didenko.ru/}project" minOccurs="0"/&gt;
 *         &lt;element name="newDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "editDescriptionProject", propOrder = {
    "arg0",
    "entity",
    "newDescription"
})
public class EditDescriptionProject {

    protected Session arg0;
    protected Project entity;
    protected String newDescription;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link Session }
     *     
     */
    public Session getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Session }
     *     
     */
    public void setArg0(Session value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link Project }
     *     
     */
    public Project getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Project }
     *     
     */
    public void setEntity(Project value) {
        this.entity = value;
    }

    /**
     * Gets the value of the newDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewDescription() {
        return newDescription;
    }

    /**
     * Sets the value of the newDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewDescription(String value) {
        this.newDescription = value;
    }

}
