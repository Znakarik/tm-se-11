package ru.didenko.tm.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TerminalService {

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public String nextLine() throws IOException {
        return reader.readLine();
    }
}
