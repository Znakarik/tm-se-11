package ru.didenko.tm.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static java.security.MessageDigest.getInstance;

public final class HasherPassword {
    static MessageDigest md;

    public HasherPassword() throws NoSuchAlgorithmException {
    }

    public static String fromStringToHash(String password) throws NoSuchAlgorithmException {
        md = getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    public static boolean checkPasswords(String first, String second) throws NoSuchAlgorithmException {
        return fromStringToHash(first).equals(fromStringToHash(second));
    }
}
