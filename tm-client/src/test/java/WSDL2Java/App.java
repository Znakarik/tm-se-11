package WSDL2Java;

import javax.xml.transform.Source;

import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class App {

    static URL wsdlURL;
    static QName SERVICE_NAME = new QName("http://WSDL2Java/", "SumService");
    static QName SERVICE_PORT = new QName("http://WSDL2Java/", "SumPort");

    static {
        try {
            wsdlURL = new URL("http://localhost:8081/sumService?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public App() throws MalformedURLException {
    }

    public static void main(String[] args) {

        Sum math = new Sum();
        Endpoint.publish(String.valueOf(App.wsdlURL), math);
        Service service = Service.create(wsdlURL, SERVICE_NAME);
//        service.getPort(SERVICE_PORT, SumEndpoint.class);
        Dispatch<Source> dispatch = service.createDispatch(SERVICE_PORT, Source.class, Service.Mode.PAYLOAD);

        Source req = new StreamSource("<hello/>");
        Source resp = dispatch.invoke(req);
    }
}
