package WSDL2Java;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class Sum implements SumEndpoint {

    @WebMethod
    public double sum(@WebParam int a, @WebParam int b) {
        return a + b;
    }
}
