package WSDL2Java;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = "http://WSDL2Java/", name = "SumPort")
public interface SumEndpoint {

    @WebMethod
    double sum(@WebParam int a, @WebParam int b);
}
