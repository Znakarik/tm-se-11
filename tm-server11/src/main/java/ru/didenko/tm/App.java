package ru.didenko.tm;

import ru.didenko.tm.boostrap.Boostrap;

public class App {

    public static void main(String[] args) throws Exception {
        Boostrap boostrap = new Boostrap();
        boostrap.init();
    }
}
