package ru.didenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Session;

import javax.jws.WebMethod;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;

public interface IProjectEndpoint<T extends Project> {

    @WebMethod
    Collection<T> findAllProject(@Nullable final Session session) throws Exception;

    @WebMethod
    List<T> findListProject(@Nullable final Session session) throws Exception;

    @WebMethod
    List<T> addAllProject(@Nullable final Session session, @NotNull List<T> entity) throws Exception;

    @WebMethod
    void removeAllProject(@Nullable final Session session) throws Exception;

    @WebMethod
    void showAllProject(@Nullable final Session session) throws Exception;

    @WebMethod
    void deleteProject(@Nullable final Session session, @NotNull final String name) throws Exception;

    @WebMethod
    List<T> getListProject(@Nullable final Session session) throws Exception;

    @WebMethod
    T searchProject(@Nullable final Session session, final @NotNull String searchVal) throws Exception;

    @WebMethod
    List<T> getSecureListProject(@Nullable final Session session, @NotNull final String userId) throws Exception;

    @WebMethod
    void sortByStartProject(@Nullable final Session session, @Nullable List<T> list) throws Exception;

    @WebMethod
    void sortByCreationProject(@Nullable final Session session, @Nullable List<T> list) throws Exception;

    @WebMethod
    void sortByFinishProject(@Nullable final Session session, @Nullable List<T> list) throws Exception;

    @WebMethod
    void sortByStatusProject(@Nullable final Session session, @Nullable List<T> list) throws Exception;

    @WebMethod
    T findOneByNameProject(@Nullable final Session session, @NotNull final String name) throws Exception;

    @WebMethod
    void editDescriptionProject(@Nullable final Session session, final @NotNull T entity, final @NotNull String newDescription) throws Exception;

    @WebMethod
    void createNewProject(@Nullable final Session session, final @NotNull String name, final @Nullable String description, final @Nullable String start, final @Nullable String finish) throws ParseException, Exception;
    
    @WebMethod
    Project secureGetOneProject(@Nullable final Session session, @NotNull String userId, @NotNull String name) throws Exception;
}
