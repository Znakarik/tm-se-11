package ru.didenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.Session;

import javax.jws.WebMethod;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;

public interface ISessionEndpoint<T extends Session> {


    @WebMethod
    Session createNewSession(
            @NotNull final String login,
            @NotNull final String pass) throws NoSuchAlgorithmException;

    @WebMethod
    void removeSession(@NotNull final String userId, @NotNull final String id);

}
