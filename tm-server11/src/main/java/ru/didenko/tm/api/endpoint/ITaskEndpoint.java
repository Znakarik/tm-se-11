package ru.didenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.entity.Task;

import javax.jws.WebMethod;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;

public interface ITaskEndpoint<T extends Task> {

    @WebMethod
    Collection<T> findAllTask(@NotNull final Session session) throws Exception;

    @WebMethod
    List<T> findListTask(@NotNull final Session session) throws Exception;

    @WebMethod
    List<T> addAllTask(@NotNull final Session session, @NotNull List<T> entity) throws Exception;

    @WebMethod
    void removeAllTask(@NotNull final Session session) throws Exception;

    @WebMethod
    void showAllTask(@NotNull final Session session) throws Exception;

    @WebMethod
    void deleteTask(@NotNull final Session session, @NotNull final String name) throws Exception;

    @WebMethod
    List<T> getListTask(@NotNull final Session session) throws Exception;

    @WebMethod
    T searchTask(@NotNull final Session session, final @NotNull String searchVal) throws Exception;

    @WebMethod
    List<T> getSecureListTask(@NotNull final Session session, @NotNull final String userId) throws Exception;

    @WebMethod
    void sortByStartTask(@NotNull final Session session, @Nullable final List<T> list) throws Exception;

    @WebMethod
    void sortByCreationTask(@NotNull final Session session, @Nullable final List<T> list) throws Exception;

    @WebMethod
    void sortByFinishTask(@NotNull final Session session, @Nullable final List<T> list) throws Exception;

    @WebMethod
    void sortByStatusTask(@NotNull final Session session, @Nullable final List<T> list) throws Exception;

    @WebMethod
    T findOneByNameTask(@NotNull final Session session, @NotNull final String name) throws Exception;

    @WebMethod
    void editDescriptionTask(@NotNull final Session session, final @NotNull T entity, final @NotNull String newDescription) throws Exception;

    @WebMethod
    void createNewTask(@NotNull final Session session, final @NotNull String name, final @Nullable String description, final @Nullable String start, final @Nullable String finish) throws ParseException, Exception;

    @WebMethod
    void setProjectTask(@NotNull final Session session, @NotNull final String taskName, @NotNull final String projectId) throws Exception;

    @WebMethod
    Task secureGetOneTask(@Nullable final Session session, @NotNull String userId, @NotNull String name) throws Exception;
}
