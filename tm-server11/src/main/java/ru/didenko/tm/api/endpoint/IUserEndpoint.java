package ru.didenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.entity.User;

import javax.jws.WebMethod;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;

public interface IUserEndpoint<T extends User> {

    @WebMethod
    Collection<T> findAllUser(@NotNull final Session session) throws Exception;

    @WebMethod
    List<T> findListUser(@NotNull final Session session) throws Exception;

    @WebMethod
    List<T> addAllUser(@NotNull final Session session, @NotNull List<T> entity) throws Exception;

    @WebMethod
    void removeAllUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void showAllUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void deleteUser(@NotNull final Session session, @NotNull final String name) throws Exception;

    @WebMethod
    List<T> getListUser(@NotNull final Session session) throws Exception;

//    @WebMethod
//    User getCurrentUser(@NotNull final Session session) throws Exception;
//
//    @WebMethod
//    void setCurrentUser(@Nullable final Session session, final @Nullable User currentUser) throws Exception;

    @WebMethod
    void createUser(@NotNull final Session session, final @NotNull String login, final @NotNull String pass, final @NotNull Role role) throws Exception;

    @WebMethod
    void setCurrentUserByName(@NotNull final Session session, final @NotNull String name) throws Exception;

    @WebMethod
    User findUser(@NotNull final Session session, final @NotNull String login, final @NotNull String pass) throws Exception;

    @WebMethod
    User findOneByNameUser(@NotNull final Session session, @NotNull final String name) throws Exception;

    @WebMethod
    void binSaveUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void jsonJacksonSaveUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void xmlJacksonSaveUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void jsonJaxbSaveUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void xmlJaxbSaveUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void binLoadUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void jsonJacksonLoadUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void jsonJaxbLoadUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void xmlJacksonLoadUser(@NotNull final Session session) throws Exception;

    @WebMethod
    void xmlJaxbLoadUser() throws Exception;
}
