package ru.didenko.tm.api.project;

import ru.didenko.tm.api.repository.IGoalRepository;
import ru.didenko.tm.entity.Project;

public interface IProjectRepository extends IGoalRepository<Project> {

}
