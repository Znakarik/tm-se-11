package ru.didenko.tm.api.project;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.api.service.IGoalService;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;

import javax.jws.WebMethod;

public interface IProjectService extends IGoalService<Project> {
    
    Project secureGetOne(@NotNull String userId, @NotNull String name) throws Exception;
}
