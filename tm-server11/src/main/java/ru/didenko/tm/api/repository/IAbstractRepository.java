package ru.didenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IAbstractRepository<T extends AbstractEntity> {
    Collection<T> findAll();

    T findOne(String id);

//    T findOneByName(String name);

    T persist(T entity);

    List<T> mergeAll(@NotNull List<T> entity);

    T merge(T entity);

    void remove(String id);

    void removeAll();
}
