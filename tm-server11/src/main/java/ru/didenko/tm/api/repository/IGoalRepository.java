package ru.didenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.entity.AbstractGoal;

import java.util.List;

public interface IGoalRepository<T extends AbstractGoal> extends IAbstractRepository<T> {

    T findOneWithUserId(@NotNull String userId, @NotNull String name);

    T secureFindOneByName(String name, String userId);

    List<T> findAllWithUserId(@NotNull String userId);
}
