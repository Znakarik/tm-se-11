package ru.didenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IAbstractService<T extends AbstractEntity> {

    Collection<T> findAll();

    List<T> findList();

    List<T> addAll(@NotNull List<T> entity);

    void removeAll();

    void showAll();

    void delete(String name);

    List<T> getList();

    T persist(T entity);

}
