package ru.didenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.AbstractEntity;
import ru.didenko.tm.entity.AbstractGoal;

import java.text.ParseException;
import java.util.List;

public interface IGoalService<T extends AbstractGoal> extends IAbstractService<T> {

    T search(final @NotNull String searchVal);

    List<T> getSecureList(String userId);

    void sortByStart(List<T> list);

    void sortByCreation(List<T> list);

    void sortByFinish(List<T> list);

    void sortByStatus(List<T> list);

    T findOneByName(String name);

    void editDescription(final @NotNull T entity, final @NotNull String newDescription);

    void createNew(final @NotNull String name, final @Nullable String description, final @Nullable String start, final @Nullable String finish) throws ParseException;
}
