package ru.didenko.tm.api.service;

import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.service.ProjectService;
import ru.didenko.tm.service.TaskService;
import ru.didenko.tm.service.TerminalService;
import ru.didenko.tm.service.UserService;

import java.util.Map;

public interface ServiceLocator {

    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    Map<String,? extends AbstractCommand<?>> getCommandMap();

    TerminalService getTerminalService();
}
