package ru.didenko.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.repository.IAbstractRepository;
import ru.didenko.tm.entity.Session;

public interface ISessionRepository extends IAbstractRepository<Session> {

    @Nullable Session findOne(@NotNull String userID, @NotNull String id);

    @Nullable void remove(@NotNull String userID, @NotNull String id);


}
