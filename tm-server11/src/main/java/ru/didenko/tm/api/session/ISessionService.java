package ru.didenko.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.IAbstractService;
import ru.didenko.tm.entity.Session;

public interface ISessionService extends IAbstractService<Session> {

    void remove(@NotNull String id, @NotNull String userId);

    @Nullable Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception;
}
