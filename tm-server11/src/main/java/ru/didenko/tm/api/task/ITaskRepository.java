package ru.didenko.tm.api.task;

import ru.didenko.tm.api.repository.IGoalRepository;
import ru.didenko.tm.entity.Task;

public interface ITaskRepository extends IGoalRepository<Task> {

}
