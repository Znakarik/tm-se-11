package ru.didenko.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.IGoalService;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.entity.Task;

public interface ITaskService extends IGoalService<Task> {

    void setProject(String taskName, String projectId);

    Task secureGetOne(@NotNull String userId, @NotNull String name) throws Exception;

}
