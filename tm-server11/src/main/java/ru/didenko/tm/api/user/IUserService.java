package ru.didenko.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.IAbstractService;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.User;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IUserService extends IAbstractService<User> {

//    User getCurrentUser();
//
//    void setCurrentUser(final @Nullable User currentUser);

    User createUser(final @NotNull String login, final @NotNull String pass, final @NotNull Role role) throws NoSuchAlgorithmException;

    User loadUser(final @NotNull String login, final @NotNull String pass, final @NotNull Role role);

    void loadUsers(@Nullable final List<User> users);

    void setCurrentUserByName(final @Nullable String name);

    User findUser(final @NotNull String login, final @NotNull String pass) throws NoSuchAlgorithmException;

    User findOneByName(String name);

    void binSave() throws IOException;

    void jsonJacksonSave() throws IOException;

    void xmlJacksonSave() throws IOException;

    void jsonJaxbSave() throws JAXBException;

    void xmlJaxbSave() throws JAXBException;

    void binLoad() throws IOException, ClassNotFoundException;

    void jsonJacksonLoad() throws IOException;

    void jsonJaxbLoad() throws JAXBException;

    void xmlJacksonLoad() throws IOException;

    void xmlJaxbLoad() throws IOException, JAXBException;
}

