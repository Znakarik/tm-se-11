package ru.didenko.tm.boostrap;

import com.sun.istack.internal.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.reflections.Reflections;
import ru.didenko.tm.api.service.ServiceLocator;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.endpoint.ProjectEndpoint;
import ru.didenko.tm.endpoint.SessionEndpoint;
import ru.didenko.tm.endpoint.TaskEndpoint;
import ru.didenko.tm.endpoint.UserEndpoint;
import ru.didenko.tm.repository.ProjectRepository;
import ru.didenko.tm.repository.SessionRepository;
import ru.didenko.tm.repository.TaskRepository;
import ru.didenko.tm.repository.UserRepository;
import ru.didenko.tm.service.*;

import javax.xml.bind.JAXBException;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.util.*;

@Data
@NoArgsConstructor
public final class Boostrap implements ServiceLocator {

    @NotNull
    final TerminalService terminalService = new TerminalService();
    @NotNull
    final ProjectService projectService = new ProjectService(new ProjectRepository());
    @NotNull
    final TaskService taskService = new TaskService(new TaskRepository());
    @NotNull
    final UserService userService = new UserService(new UserRepository(), projectService, taskService);
    @NotNull
    final SessionService sessionService = new SessionService(new SessionRepository());
    @NotNull
    final Map<String, AbstractCommand<? extends AbstractCommand<?>>> commandMap = new LinkedHashMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes1 = new Reflections("ru.didenko.tm").getSubTypesOf(ru.didenko.tm.command.AbstractCommand.class);

    ProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, projectService);
    TaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService);
    UserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);
    SessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService);

    public void loadData() throws IOException, JAXBException, ClassNotFoundException {
        userService.binLoad();
    }

    public final void publish() {
        Endpoint.publish("http://localhost:8081/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8081/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8081/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8081/sessionService?wsdl", sessionEndpoint);
    }

    public final void init() throws Exception {
        loadData();
        publish();
    }

    @Override
    public ProjectService getProjectService() {
        return null;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public Map<String, ? extends AbstractCommand<?>> getCommandMap() {
        return commandMap;
    }

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }
}