package ru.didenko.tm.command;

import lombok.NoArgsConstructor;
import ru.didenko.tm.api.service.ServiceLocator;
import ru.didenko.tm.service.TerminalService;

@NoArgsConstructor
public abstract class AbstractCommand<T extends AbstractCommand<T>> {
    protected ServiceLocator serviceLocator;
    protected TerminalService terminalService;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean isSecure();

}
