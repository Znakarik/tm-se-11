package ru.didenko.tm.command;

import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.util.Assertion;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public class SearchCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "search";
    }

    @Override
    public String getDescription() {
        return "Search project or task";
    }

    @Override
    public void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException {
        System.out.println("[SEARCH]");
        System.out.println("ENTER NAME OR PART OF DESCRIPTION");
        String searchChoice = serviceLocator.getTerminalService().nextLine();
        if (!Assertion.assertNotNull(searchChoice)) return;
        Project project = serviceLocator.getProjectService().search(searchChoice);
        if (project != null) {
            System.out.println(project.toString());
        } else if (project == null) {
            Task task = serviceLocator.getTaskService().search(searchChoice);
            if (task != null) System.out.println(task.toString());
            else if (task == null) {
                System.err.println("THERE'S NO PROJECT OR TASK");
            }
        }
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
