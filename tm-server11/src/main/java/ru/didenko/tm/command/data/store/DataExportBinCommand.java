package ru.didenko.tm.command.data.store;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.save.DataExporter;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class DataExportBinCommand extends AbstractCommand<DataExportBinCommand> {

    @Override
    public String getName() {
        return "save-bin";
    }

    @Override
    public String getDescription() {
        return "Save data in binary format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().binSave();
        System.out.println("[DATA SAVED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
