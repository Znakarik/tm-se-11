package ru.didenko.tm.command.data.store;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.save.DataExporter;

import java.io.File;

public class DataExportJsonJacksonCommand extends AbstractCommand<DataExportJsonJacksonCommand> {
    @Override
    public String getName() {
        return "save-json-jackson";
    }

    @Override
    public String getDescription() {
        return "Save data in JSON using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().jsonJacksonSave();
        System.out.println("[DATA SAVED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
