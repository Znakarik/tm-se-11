package ru.didenko.tm.command.data.store;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.save.DataExporter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class DataExportJsonJaxbCommand extends AbstractCommand<DataExportJsonJaxbCommand> {
    @Override
    public String getName() {
        return "save-jaxb-json";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().jsonJaxbSave();
        System.out.println("[DATA SAVED]");

    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
