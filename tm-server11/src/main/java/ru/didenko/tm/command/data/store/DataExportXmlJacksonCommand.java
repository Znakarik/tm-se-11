package ru.didenko.tm.command.data.store;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.save.DataExporter;

import java.io.File;

public class DataExportXmlJacksonCommand extends AbstractCommand<DataExportXmlJacksonCommand> {
    @Override
    public String getName() {
        return "save-xml-jackson";
    }

    @Override
    public String getDescription() {
        return "Save data in XML using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().xmlJacksonSave();
        System.out.println("[DATA SAVED]");


    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
