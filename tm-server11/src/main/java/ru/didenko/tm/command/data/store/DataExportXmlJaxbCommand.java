package ru.didenko.tm.command.data.store;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.save.DataExporter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class DataExportXmlJaxbCommand extends AbstractCommand<DataExportXmlJaxbCommand> {
    @Override
    public String getName() {
        return "save-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Save in XML format using JAXB";
    }

    @Override
    public void execute() throws JAXBException {
        serviceLocator.getUserService().xmlJaxbSave();
        System.out.println("[DATA SAVED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
