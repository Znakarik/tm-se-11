package ru.didenko.tm.command.data.upload;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.load.DataImporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataImportDataBinCommand extends AbstractCommand<DataImportDataBinCommand> {
    @Override
    public String getName() {
        return "load-data-bin";
    }

    @Override
    public String getDescription() {
        return "Load data from binary";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().binLoad();
        System.out.println("[DATA LOADED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
