package ru.didenko.tm.command.data.upload;

import ru.didenko.tm.command.AbstractCommand;

import java.io.File;

public class DataImportJSONJacksonCommand extends AbstractCommand<DataImportJSONJacksonCommand> {
    @Override
    public String getName() {
        return "load-json-jackson";
    }

    @Override
    public String getDescription() {
        return "Load data from JSON using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().jsonJacksonLoad();
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
