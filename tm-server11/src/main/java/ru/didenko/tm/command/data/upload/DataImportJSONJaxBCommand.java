package ru.didenko.tm.command.data.upload;

import ru.didenko.tm.command.AbstractCommand;

public class DataImportJSONJaxBCommand extends AbstractCommand<DataImportJSONJaxBCommand> {

    @Override
    public String getName() {
        return "load-json-jaxb";
    }

    @Override
    public String getDescription() {
        return "Load data from JSON using JAXB";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().jsonJaxbLoad();
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
