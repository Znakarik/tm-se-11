package ru.didenko.tm.command.data.upload;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.load.DataImporter;

public class DataImportXMLJacksonCommand extends AbstractCommand<DataImportXMLJacksonCommand> {
    @Override
    public String getName() {
        return "load-xml-jackson";
    }

    @Override
    public String getDescription() {
        return "Load data from XML using FasterXML";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().xmlJacksonLoad();
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
