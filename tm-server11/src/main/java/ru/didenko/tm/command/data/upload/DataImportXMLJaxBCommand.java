package ru.didenko.tm.command.data.upload;

import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.util.data.load.DataImporter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;

public class DataImportXMLJaxBCommand extends AbstractCommand<DataImportXMLJaxBCommand> {
    @Override
    public String getName() {
        return "load-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Load data from XML using JAXB";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().xmlJaxbLoad();
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
