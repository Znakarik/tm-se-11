package ru.didenko.tm.command.help;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand<HelpCommand> {

    @Override
    public final String getName() {
        return "help";
    }

    @Override
    public final String getDescription() {
        return "Show all commands";
    }

    @Override
    public final void execute() {
        for (@NotNull final AbstractCommand<?> command : serviceLocator.getCommandMap().values()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}

