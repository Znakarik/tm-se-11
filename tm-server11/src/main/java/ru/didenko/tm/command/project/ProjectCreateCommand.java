package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.text.ParseException;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand<ProjectCreateCommand> {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws IOException, ParseException, NullPointerException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        @Nullable User currentUSer = serviceLocator.getUserService().getCurrentUser();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = reader.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = reader.nextLine();
        System.out.println("ENTER START DATE IN FORMAT : DD.MM.YYYY");
        final String startDate = reader.nextLine();
        System.out.println("ENTER FINISH DATE IN FORMAT : DD.MM.YYYY");
        final String finishDate = reader.nextLine();
        serviceLocator.getProjectService().createNew(name, description, startDate, finishDate);
        if (currentUSer.getRole() == Role.ADMIN) {
            System.out.println("HELLO ADMIN. SET PROJECT TO YOUR PROFILE? y/n");
            final String answer = reader.nextLine();
            if (answer.equals("y")) {
                serviceLocator.getProjectService().findOneByName(name).setUserId(currentUSer.getId());
            }
        } else {
            Project project = serviceLocator.getProjectService().findOneByName(name);
            project.setUserId(currentUSer.getId());
        }
        System.out.println("[PROJECT CREATED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
