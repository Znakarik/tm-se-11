package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.service.AbstractGoalService;
import ru.didenko.tm.service.ProjectService;
import ru.didenko.tm.util.Assertion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand<ProjectListCommand> {

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws IOException {
        @Nullable User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("[PROJECT LIST]");
        @NotNull int index = 1;
        System.out.println("SHOW PROJECTS BY: START, FINISH, CREATION, STATUS?");
        final @NotNull String choice = serviceLocator.getTerminalService().nextLine();
        @Nullable List<Project> projects = new ArrayList<>();

        if (currentUser.getRole() == Role.USER) {
            projects.addAll(serviceLocator.getProjectService().getSecureList(currentUser.getId()));
        } else if (currentUser.getRole() == Role.ADMIN) {
            projects.addAll(serviceLocator.getProjectService().getList());
        }

        /**
         * TODO Попробовать внедрить в {@link AbstractGoalService#getList()}
         * {@link ProjectService#secureGetAll(String)}
         */
        if (!Assertion.assertNotNull(projects)) return;
        switch (choice) {
            case "start":
                serviceLocator.getProjectService().sortByStart(projects);
                break;
            case "finish":
                serviceLocator.getProjectService().sortByFinish(projects);
                break;
            case "creation":
                serviceLocator.getProjectService().sortByCreation(projects);
            case "status":
                serviceLocator.getProjectService().sortByStatus(projects);
                break;
            default:
                System.err.println("TRY AGAIN");
                execute();
                break;
        }

        if (!Assertion.assertNotNull(projects)) return;
        for (@NotNull Project project : projects) {
            System.out.println(index++ + ". " + project.getName());
        }

    }


    @Override
    public boolean isSecure() {
        return false;
    }
}
