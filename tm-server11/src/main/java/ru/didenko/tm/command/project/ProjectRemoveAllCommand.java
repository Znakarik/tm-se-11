package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.entity.User;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

@NoArgsConstructor
public final class ProjectRemoveAllCommand extends AbstractCommand<ProjectRemoveAllCommand> {

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws IOException, ParseException {
        System.out.println("[PROJECT CLEAR]");
        Iterator<Task> taskIterator = serviceLocator.getTaskService().findAll().iterator();
        Iterator<Project> projectIterator = serviceLocator.getProjectService().findAll().iterator();
        while (projectIterator.hasNext()) {
            Project project = projectIterator.next();
            @Nullable User currentUser = serviceLocator.getUserService().getCurrentUser();
            if (currentUser.getRole() == Role.ADMIN) {
                while (taskIterator.hasNext()) {
                    Task task = taskIterator.next();
                    // Логика под админа - может удалять все проекты
                    if (project.getId().equals(task.getProjectId())) {
                        taskIterator.remove();
                    }
                }
                projectIterator.remove();
            }

            // Юзер может удалять только свои проекты
            if ((currentUser.getRole() == Role.USER)) {
                if (serviceLocator.getTaskService().findAll().size() > 0) {
                    while (taskIterator.hasNext()) {
                        if (project.getId().equals(taskIterator.next().getProjectId()) && project.getUserId().equals(currentUser.getId())) {
                            taskIterator.remove();
                        }
                    }
                }
                if (project.getUserId().equals(currentUser.getId())) {
                    projectIterator.remove();
                }
            }
        }
        System.out.println("[PROJECTS DELETED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
