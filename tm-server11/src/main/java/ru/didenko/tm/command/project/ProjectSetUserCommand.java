package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@NoArgsConstructor
public final class ProjectSetUserCommand extends AbstractCommand<ProjectSetUserCommand> {

    @Override
    public String getName() {
        return "project-set-user";
    }

    @Override
    public String getDescription() {
        return "Set user to project";
    }

    @Override
    public void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        System.out.println("[SET USER TO PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        final String projectName = reader.nextLine();
        System.out.println("ENTER USER LOGIN");
        final String userName = reader.nextLine();
        serviceLocator.getProjectService().findOneByName(projectName)
                .setUserId(serviceLocator.getUserService().findOneByName(userName).getId());
        System.out.println("[USER SET TO PROJECT]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
