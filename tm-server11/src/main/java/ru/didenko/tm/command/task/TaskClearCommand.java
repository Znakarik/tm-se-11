package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;

import java.io.IOException;
import java.text.ParseException;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand<TaskClearCommand> {

    @Override
    public final String getName() {
        return "task-clear";
    }

    @Override
    public final String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public final void execute() throws IOException, ParseException {
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskService().removeAll();
        System.out.println("[ALL TASK DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
