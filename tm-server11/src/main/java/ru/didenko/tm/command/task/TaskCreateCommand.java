package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.text.ParseException;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand<TaskCreateCommand> {

    @Override
    public final String getName() {
        return "task-create";
    }

    @Override
    public final String getDescription() {
        return "Create new task";
    }

    @Override
    public final void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = reader.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = reader.nextLine();
        System.out.println("ENTER START DATE IN FORMAT : DD.MM.YYYY");
        final String startDate = reader.nextLine();
        System.out.println("ENTER FINISH DATE IN FORMAT : DD.MM.YYYY");
        final String finishDate = reader.nextLine();
        serviceLocator.getTaskService().createNew(name, description, startDate, finishDate);
        @Nullable User current = serviceLocator.getUserService().getCurrentUser();
        if (current.getRole() == Role.USER) {
            serviceLocator.getTaskService().findOneByName(name).setUserId(current.getId());
        }
        if (current.getRole() == Role.ADMIN) {
            System.out.println("HELLO ADMIN. SET TASK TO YOUR PROFILE? y/n");
            final String answer = reader.nextLine();
            if (answer.equals("y")) {
                serviceLocator.getTaskService().findOneByName(name).setUserId(current.getId());
            }
        }

        System.out.println("[TASK CREATED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
