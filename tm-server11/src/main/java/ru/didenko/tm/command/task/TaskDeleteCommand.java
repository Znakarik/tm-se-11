package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.util.Iterator;

@NoArgsConstructor
public final class TaskDeleteCommand extends AbstractCommand<TaskDeleteCommand> {

    @Override
    public final String getName() {
        return "task-remove";
    }

    @Override
    public final String getDescription() {
        return "Remove selected task";
    }

    @Override
    public final void execute() throws IOException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        System.out.println("ENTER PROJECT NAME");
        final String projectName = reader.nextLine();
        System.out.println("ENTER TASK NAME");
        final String taskName = reader.nextLine();
        final Iterator<Project> iterator = serviceLocator.getProjectService().findAll().iterator();
        final Iterator<Task> taskIterator = serviceLocator.getTaskService().findAll().iterator();
        @Nullable User current = serviceLocator.getUserService().getCurrentUser();
        while (iterator.hasNext()) {
            final String p = iterator.next().getName();
            if (p.equals(projectName)) {
                while (taskIterator.hasNext()) {
                    final Task task = taskIterator.next();
                    if (current.getRole() == Role.ADMIN) {
                        if (task.getName().equals(taskName)) {
                            taskIterator.remove();
                        }
                    }
                    if (current.getRole() == Role.USER) {
                        if (task.getUserId().equals(current.getId())) {
                            taskIterator.remove();
                        }
                    }
                }
            }
        }
        System.out.println("[TASK DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}