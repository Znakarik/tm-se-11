package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.util.Assertion;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand<TaskListCommand> {

    @Override
    public final String getName() {
        return "task-list";
    }

    @Override
    public final String getDescription() {
        return "Show all tasks";
    }

    @Override
    public final void execute() throws IOException {
        System.out.println("[TASK LIST]");
        @NotNull int index = 1;
        System.out.println("SHOW TASKS BY: START, FINISH, CREATION, STATUS?");
        final @NotNull String choice = terminalService.nextLine();
        List<Task> tasks = null;
        User currentUser = serviceLocator.getUserService().getCurrentUser();

        if (currentUser.getRole() == Role.USER) {
            tasks = serviceLocator.getTaskService().getSecureList(currentUser.getId());
        } else if (currentUser.getRole() == Role.ADMIN) tasks = serviceLocator.getTaskService().getList();

        assert tasks != null;
        switch (choice) {
            case "start":
                serviceLocator.getTaskService().sortByStart(tasks);
                break;
            case "finish":
                serviceLocator.getTaskService().sortByFinish(tasks);
                break;
            case "creation":
                serviceLocator.getTaskService().sortByCreation(tasks);
                break;
            case "status":
                serviceLocator.getTaskService().sortByStatus(tasks);
                break;
            default:
                System.err.println("REPEAT PLZ");
                execute();
                break;
        }
        if (!Assertion.assertNotNull(tasks)) return;

        for (@NotNull Task task : tasks) {
            System.out.println(index++ + ". " + task.getName());
        }
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
