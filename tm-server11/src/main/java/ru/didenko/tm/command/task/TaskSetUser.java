package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class TaskSetUser extends AbstractCommand<TaskSetUser> {

    @Override
    public final String getName() {
        return "task-set-user";
    }

    @Override
    public final String getDescription() {
        return "Set user to task";
    }

    @Override
    public final void execute() throws IOException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        System.out.println("[SET TASK TO USER]");
        System.out.println("ENTER TASK NAME");
        final String taskName = reader.nextLine();
        System.out.println("[ENTER USER LOGIN]");
        final String userName = reader.nextLine();
        serviceLocator.getTaskService().findOneByName(taskName).setUserId(serviceLocator.getUserService().getCurrentUser().getId());
        System.out.println("[TASK HAS SET]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
