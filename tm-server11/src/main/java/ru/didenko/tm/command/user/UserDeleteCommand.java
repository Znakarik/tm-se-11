package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.util.Iterator;

@NoArgsConstructor
public final class UserDeleteCommand extends AbstractCommand<UserDeleteCommand> {

    @Override
    public final String getName() {
        return "user-delete";
    }

    @Override
    public final String getDescription() {
        return "Remove selected user ";
    }

    @Override
    public final void execute() throws IOException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        System.out.println("[DELETE USER]");
        System.out.println("ENTER LOGIN");
        final String login = reader.nextLine();
        final Iterator<Project> projectIterator = serviceLocator.getProjectService().findAll().iterator();
        final Iterator<Task> taskIterator = serviceLocator.getTaskService().findAll().iterator();
        // Если есть привязанные проект к юзеру - удаляем
        while (projectIterator.hasNext()) {
            if (projectIterator.next().getUserId().equals(serviceLocator.getUserService().getCurrentUser().getId())) {
                projectIterator.remove();
            }

        }
        // Если есть привязанные таски к юзеру - удаляем
        while (taskIterator.hasNext()) {

            if (taskIterator.next().getUserId().equals(serviceLocator.getUserService().getCurrentUser().getId())) {
                taskIterator.remove();
            }
            if (taskIterator.hasNext()) taskIterator.next();
        }
        // Создаем условия для разлога, если удаляем себя
        User user = null;
        serviceLocator.getUserService().

                delete(login);
        if (login.equals(serviceLocator.getUserService().
                getCurrentUser().getName())) {
            serviceLocator.getUserService().setCurrentUser(user);
        }
        System.out.println("[USER DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}