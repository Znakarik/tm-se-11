package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@NoArgsConstructor
public final class UserEditLoginCommand extends AbstractCommand<UserEditLoginCommand> {

    @Override
    public final String getName() {
        return "user-edit-name";
    }

    @Override
    public final String getDescription() {
        return "Edit user name";
    }

    @Override
    public final void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        System.out.println("[EDIT USER LOGIN]");
        System.out.println("ENTER NEW LOGIN");
        final String newUserName = reader.nextLine();
        serviceLocator.getUserService().getCurrentUser().setName(newUserName);
        System.out.println("[LOGIN CHANGED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
