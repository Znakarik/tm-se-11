package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.entity.User;

import java.io.IOException;
import java.text.ParseException;

@NoArgsConstructor
public final class UserListCommand extends AbstractCommand<UserListCommand> {

    @Override
    public final String getName() {
        return "user-list";
    }

    @Override
    public final String getDescription() {
        return "Show all users";
    }

    @Override
    public final void execute() throws IOException, ParseException {
        System.out.println("[USER LIST]");
        int index = 1;
        for (User user : serviceLocator.getUserService().findAll()) {
            System.out.println(index++ + ". " + user.getName());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
