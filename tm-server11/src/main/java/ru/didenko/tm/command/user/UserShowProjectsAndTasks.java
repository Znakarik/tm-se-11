package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@NoArgsConstructor
public class UserShowProjectsAndTasks extends AbstractCommand<UserShowProjectsAndTasks> {

    @Override
    public final String getName() {
        return "user-projects&tasks";
    }

    @Override
    public final String getDescription() {
        return "Show my projects and tasks";
    }

    @Override
    public final void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException {
        serviceLocator.getProjectService().findAll().forEach(project -> {
            if (project.getUserId().equals(serviceLocator.getUserService().getCurrentUser().getId())) {
                System.out.println(project.toString());
                serviceLocator.getTaskService().findAll().forEach(task -> {
                    if (task.getProjectId().equals(project.getId())) {
                        System.out.println(task.toString());
                    }
                });
            } else System.err.println("THERE'RE NO YOUR'S PROJECT");
        });
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
