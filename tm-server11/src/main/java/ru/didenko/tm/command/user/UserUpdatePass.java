package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@NoArgsConstructor
public final class UserUpdatePass extends AbstractCommand<UserUpdatePass> {

    @Override
    public final String getName() {
        return "user-pass-upd";
    }

    @Override
    public final String getDescription() {
        return "Update password for current user";
    }

    @Override
    public final void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException {
        @NotNull final TerminalService reader = serviceLocator.getTerminalService();
        System.out.println("[UPDATE PASS]");
        System.out.println("ENTER NEW PASSWORD");
        final String newPass = reader.nextLine();
        serviceLocator.getUserService().getCurrentUser().setPassword(newPass);
        System.out.println("[PASSWORD UPDATED]");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}