package ru.didenko.tm.constant;

import org.jetbrains.annotations.NotNull;

public class AppConstants {
    @NotNull public final static String SALT = "ksdnfnfiew";
    @NotNull public final static Integer CYCLE = 64;
}
