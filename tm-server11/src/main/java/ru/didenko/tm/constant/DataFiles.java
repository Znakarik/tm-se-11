package ru.didenko.tm.constant;

import org.jetbrains.annotations.NotNull;

/**
 * @author znakarik
 */
public enum DataFiles {

    XML_JAXB_DATA("data/jaxb.xml"),
    BIN_DATA_FILE("data/databin.ser"),
    JSON_DATA_FILE("data/jaxb.json"),
    JACKSON_XML_DATA("data/jackson.xml"),
    JACKSON_JSON_DATA("data/jackson.json");

    private String filePath;

    DataFiles(final @NotNull String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }
}
