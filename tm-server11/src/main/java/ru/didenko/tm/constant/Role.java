package ru.didenko.tm.constant;

/**
 * @author Olga Didenko
 * @version 1.6
 */
public enum Role {
    ADMIN("admin"),
    USER("user");

    private String role;

    Role(String role) {
        this.role = role;
    }

    /**
     * @return role from enum
     */
    public String displayName() {
        return role;
    }
}
