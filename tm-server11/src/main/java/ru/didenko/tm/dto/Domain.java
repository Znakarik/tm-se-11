package ru.didenko.tm.dto;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.project.IProjectService;
import ru.didenko.tm.api.service.ServiceLocator;
import ru.didenko.tm.api.task.ITaskService;
import ru.didenko.tm.api.user.IUserService;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.entity.User;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@Getter
public class Domain implements Serializable {

    private final static long serialVersionUID = 1L;

    @XmlElement(name = "projects")
    @Nullable
    private List<Project> projects;
    @XmlElement(name = "tasks")
    @Nullable
    private List<Task> tasks;
    @XmlElement(name = "users")
    @Nullable
    private List<User> users;

    public void save(ServiceLocator locator) {
        projects = locator.getProjectService().findList();
        tasks = locator.getTaskService().findList();
        users = locator.getUserService().findList();
    }

    public void load(ServiceLocator locator) {
        locator.getProjectService().addAll(projects);
        locator.getTaskService().addAll(tasks);
        locator.getUserService().addAll(users);
    }

    public void save(IProjectService projectService, ITaskService taskService, IUserService userService) {
        projects = projectService.findList();
        tasks = taskService.findList();
        users = userService.findList();
    }

    public void load(IProjectService projectService, ITaskService taskService, IUserService userService) {
        userService.loadUsers(users);
        projectService.addAll(projects);
        taskService.addAll(tasks);
    }
}
