package ru.didenko.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.session.ISessionService;
import ru.didenko.tm.constant.AppConstants;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.util.Assertion;
import ru.didenko.tm.util.data.SignatureUtil;

import javax.jws.WebService;

@NoArgsConstructor
public class AbstractEndpoint {

    @NotNull ISessionService sessionService;

    public AbstractEndpoint(@NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void accessSession(@Nullable final Session userSession) throws Exception {
        if (userSession == null) return;
        @Nullable Session session = sessionService.findOne(userSession.getUserId(), userSession.getId());
        userSession.setSignature(null);
        if (session == null) throw new Exception("Session is doesn't exist");
        if (!session.getSignature().equals(SignatureUtil.sign(userSession, AppConstants.SALT, AppConstants.CYCLE)))
            return;
    }
}
