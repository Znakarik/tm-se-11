package ru.didenko.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.endpoint.IProjectEndpoint;
import ru.didenko.tm.api.project.IProjectService;
import ru.didenko.tm.api.session.ISessionService;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint<Project> {

    @NotNull
    IProjectService service;

    public ProjectEndpoint(@NotNull final ISessionService sessionService, @NotNull final IProjectService service) {
        super(sessionService);
        this.service = service;
    }

    @WebMethod
    public Collection<Project> findAllProject(@Nullable final Session session) throws Exception {
        accessSession(session);
        return service.findAll();
    }

    @WebMethod
    @Override
    @Nullable
    public List<Project> findListProject(@Nullable final Session session) throws Exception {
        accessSession(session);
        return (List<Project>) service.findAll();
    }

    @WebMethod
    @Override
    public List<Project> addAllProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "entity") List<Project> entity) throws Exception {
        accessSession(session);
        return service.addAll(entity);
    }

    @WebMethod
    @Override
    public void removeAllProject(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.removeAll();
    }

    @WebMethod
    @Override
    public void showAllProject(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.showAll();
    }

    @WebMethod
    @Override
    public void deleteProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "name") String name) throws Exception {
        accessSession(session);
        service.delete(name);
    }

    @WebMethod
    @Override
    public List<Project> getListProject(@Nullable final Session session) throws Exception {
        accessSession(session);
        return service.getList();
    }

    @WebMethod
    @Override
    public Project searchProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "searchVal") String searchVal) throws Exception {
        accessSession(session);
        return service.search(searchVal);
    }

    @WebMethod
    @Override
    public List<Project> getSecureListProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "userId") String userId) throws Exception {
        accessSession(session);
        return service.getSecureList(userId);
    }

    @WebMethod
    @Override
    public void sortByStartProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "list") List<Project> list) throws Exception {
        accessSession(session);
        service.sortByStart(list);
    }

    @WebMethod
    @Override
    public void sortByCreationProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "list") List<Project> list) throws Exception {
        accessSession(session);
        service.sortByCreation(list);
    }

    @WebMethod
    @Override
    public void sortByFinishProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "list") List<Project> list) throws Exception {
        accessSession(session);
        service.sortByFinish(list);
    }

    @WebMethod
    @Override
    public void sortByStatusProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "list") List<Project> list) throws Exception {
        accessSession(session);
        service.sortByStatus(list);
    }

    @WebMethod
    @Override
    public Project findOneByNameProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "name") String name) throws Exception {
        accessSession(session);
        return service.findOneByName(name);
    }

    @WebMethod
    @Override
    public void editDescriptionProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "entity") Project entity,
            @NotNull @WebParam(name = "newDescription") String newDescription) throws Exception {
        accessSession(session);
        service.editDescription(entity, newDescription);
    }

    @WebMethod
    @Override
    public void createNewProject(
            @Nullable final Session session,
            @NotNull @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description,
            @Nullable @WebParam(name = "start") String start,
            @Nullable @WebParam(name = "finish") String finish) throws Exception {
        accessSession(session);
        service.createNew(name, description, start, finish);
    }

    @WebMethod
    @Override
    public Project secureGetOneProject(@Nullable final Session session, @NotNull String userId, @NotNull String name) throws Exception {
        accessSession(session);
        return service.secureGetOne(userId, name);
    }

}
