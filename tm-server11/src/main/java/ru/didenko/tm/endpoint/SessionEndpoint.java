package ru.didenko.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.endpoint.ISessionEndpoint;
import ru.didenko.tm.api.session.ISessionService;
import ru.didenko.tm.api.user.IUserService;
import ru.didenko.tm.constant.AppConstants;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.util.Assertion;
import ru.didenko.tm.util.HasherPassword;
import ru.didenko.tm.util.data.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.security.NoSuchAlgorithmException;

import static ru.didenko.tm.constant.AppConstants.CYCLE;
import static ru.didenko.tm.constant.AppConstants.SALT;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint<Session> {

    @NotNull
    private IUserService userService;

    public SessionEndpoint(@NotNull final ISessionService sessionService, @NotNull final IUserService userService) {
        super(sessionService);
        this.userService = userService;
    }

    @WebMethod
    @Override
    @Nullable
    public Session createNewSession(
            @NotNull final String log,
            @NotNull final String pass) {
        if (!Assertion.assertNotNull(log, pass)) return null;
        User user = userService.findOneByName(log);
        if (user == null) return null;
        String hashedPass = SignatureUtil.sign(pass, SALT, CYCLE);
        String userPass = user.getPassword();
        if (!userPass.equals(hashedPass)) return null;
        Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setUserName(user.getName());
        session.setSignature(SignatureUtil.sign(session, SALT, AppConstants.CYCLE));
        sessionService.persist(session);
        return session;
    }

    @WebMethod
    @Override
    public void removeSession(@NotNull final String id, @NotNull final String userId) {
        if (!Assertion.assertNotNull(id, userId)) return;
        sessionService.remove(id, userId);
    }
}
