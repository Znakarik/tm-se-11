package ru.didenko.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.endpoint.ITaskEndpoint;
import ru.didenko.tm.api.session.ISessionService;
import ru.didenko.tm.api.task.ITaskService;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.Collection;
import java.util.List;

@WebService
//@SOAPBinding(style = SOAPBinding.Style.RPC)e
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint<Task> {

    @NotNull ITaskService service;

    public TaskEndpoint(@NotNull ISessionService sessionService, @NotNull TaskService service) {
        super(sessionService);
        this.service = service;
    }

    @WebMethod
    @Override
    public Collection<Task> findAllTask(@NotNull final Session session) throws Exception {
        accessSession(session);
        return service.findAll();
    }

    @WebMethod
    @Override
    public List<Task> findListTask(@NotNull final Session session) throws Exception {
        accessSession(session);
        return service.findList();
    }

    @WebMethod
    @Override
    public List<Task> addAllTask(@NotNull final Session session,
                                 @NotNull @WebParam(name = "entity") List<Task> entity) throws Exception {
        accessSession(session);
        return service.addAll(entity);
    }

    @WebMethod
    @Override
    public void removeAllTask(@NotNull final Session session) throws Exception {
        accessSession(session);
        service.removeAll();
    }

    @WebMethod
    @Override
    public void showAllTask(@NotNull final Session session) throws Exception {
        accessSession(session);
        service.showAll();
    }

    @WebMethod
    @Override
    public void deleteTask(@NotNull final Session session,
                           @NotNull @WebParam(name = "name") String name) throws Exception {
        accessSession(session);
        service.delete(name);
    }

    @WebMethod
    @Override
    public List<Task> getListTask(@NotNull final Session session) throws Exception {
        accessSession(session);
        return service.getList();
    }

    @WebMethod
    @Override
    public Task searchTask(@NotNull final Session session,
                           @NotNull @WebParam(name = "searchVal") String searchVal) throws Exception {
        accessSession(session);
        return service.search(searchVal);
    }

    @WebMethod
    @Override
    public List<Task> getSecureListTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "userId") String userId) throws Exception {
        accessSession(session);
        return service.getSecureList(userId);
    }

    @WebMethod
    @Override
    public void sortByStartTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "list") List<Task> list) throws Exception {
        accessSession(session);
        service.sortByStart(list);
    }

    @WebMethod
    @Override
    public void sortByCreationTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "list") List<Task> list) throws Exception {
        accessSession(session);
        service.sortByCreation(list);
    }

    @WebMethod
    @Override
    public void sortByFinishTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "list") List<Task> list) throws Exception {
        accessSession(session);
        service.sortByFinish(list);
    }

    @WebMethod
    @Override
    public void sortByStatusTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "list") List<Task> list) throws Exception {
        accessSession(session);
        service.sortByStatus(list);
    }

    @WebMethod
    @Override
    public Task findOneByNameTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "name") String name) throws Exception {
        accessSession(session);
        return service.findOneByName(name);
    }

    @WebMethod
    @Override
    public void editDescriptionTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "entity") Task entity,
            @NotNull @WebParam(name = "newDescription") String newDescription) throws Exception {
        accessSession(session);
        service.editDescription(entity, newDescription);
    }

    @WebMethod
    @Override
    public void createNewTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description,
            @Nullable @WebParam(name = "start") String start,
            @Nullable @WebParam(name = "finish") String finish) throws Exception {
        accessSession(session);
        service.createNew(name, description, start, finish);
    }

    @WebMethod
    @Override
    public void setProjectTask(
            @NotNull final Session session,
            @NotNull @WebParam(name = "taskName") String taskName,
            @NotNull @WebParam(name = "projectId") String projectId) throws Exception {
        accessSession(session);
        service.setProject(taskName, projectId);
    }

    @WebMethod
    @Override
    public Task secureGetOneTask(@Nullable final Session session, @NotNull String userId, @NotNull String name) throws Exception {
        accessSession(session);
        return service.secureGetOne(userId, name);
    }
}
