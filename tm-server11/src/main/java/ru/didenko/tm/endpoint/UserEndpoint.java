package ru.didenko.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.endpoint.IUserEndpoint;
import ru.didenko.tm.api.session.ISessionService;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint<User> {

    @NotNull UserService service;

    public UserEndpoint(@NotNull ISessionService sessionService, @NotNull UserService userService) {
        super(sessionService);
        this.service = userService;
    }

    @WebMethod
    @Override
    public Collection<User> findAllUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        return service.findAll();
    }

    @WebMethod
    @Override
    public List<User> findListUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        return service.findList();
    }

    @WebMethod
    @Override
    public List<User> addAllUser(
            @Nullable final Session session,
            @NotNull @WebParam(name = "entity") List<User> entity) throws Exception {
        accessSession(session);
        service.loadUsers(entity);
        return service.getList();
    }

    @WebMethod
    @Override
    public void removeAllUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.removeAll();
    }

    @WebMethod
    @Override
    public void showAllUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.showAll();
    }

    @WebMethod
    @Override
    public void deleteUser(
            @Nullable final Session session,
            @NotNull @WebParam(name = "name") String name) throws Exception {
        accessSession(session);
        service.delete(name);
    }

    @WebMethod
    @Override
    public List<User> getListUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        return service.getList();
    }

//    @WebMethod
//    @Override
//    public User getCurrentUser(@Nullable final Session session) throws Exception {
//        accessSession(session);
//        return service.getCurrentUser();
//    }

//    @WebMethod
//    @Override
//    public void setCurrentUser(
//            @Nullable final Session session,
//            @Nullable @WebParam(name = "currentUser") User currentUser) throws Exception {
//        accessSession(session);
//        service.setCurrentUser(currentUser);
//    }

    @WebMethod
    @Override
    public void createUser(
            @Nullable final Session session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "pass") String pass,
            @NotNull @WebParam(name = "role") Role role) throws Exception {
        accessSession(session);
        service.createUser(login, pass, role);
    }

    @WebMethod
    @Override
    public void setCurrentUserByName(
            @Nullable final Session session,
            @Nullable @WebParam(name = "name") String name) throws Exception {
        accessSession(session);
        service.setCurrentUserByName(name);
    }

    @WebMethod
    @Override
    public User findUser(
            @Nullable final Session session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "pass") String pass) throws Exception {
        accessSession(session);
        return service.findUser(login, pass);
    }

    @WebMethod
    @Override
    public User findOneByNameUser(
            @Nullable final Session session,
            @NotNull @WebParam(name = "name") String name) throws Exception {
        accessSession(session);
        return service.findOneByName(name);
    }

    @WebMethod
    @Override
    public void binSaveUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.binSave();
    }

    @WebMethod
    @Override
    public void jsonJacksonSaveUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.jsonJacksonSave();
    }

    @WebMethod
    @Override
    public void xmlJacksonSaveUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.xmlJacksonSave();
    }

    @WebMethod
    @Override
    public void jsonJaxbSaveUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.jsonJaxbSave();
    }

    @WebMethod
    @Override
    public void xmlJaxbSaveUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.xmlJaxbSave();
    }

    @WebMethod
    @Override
    public void binLoadUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.binLoad();
    }

    @WebMethod
    @Override
    public void jsonJacksonLoadUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.jsonJacksonLoad();
    }

    @WebMethod
    @Override
    public void jsonJaxbLoadUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.jsonJaxbLoad();
    }

    @WebMethod
    @Override
    public void xmlJacksonLoadUser(@Nullable final Session session) throws Exception {
        accessSession(session);
        service.xmlJacksonLoad();
    }

    @WebMethod
    @Override
    public void xmlJaxbLoadUser() throws Exception {
        service.xmlJaxbLoad();
    }
}