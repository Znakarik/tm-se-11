package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Status;
import ru.didenko.tm.util.DateParser;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractGoal implements Serializable {

    private static final long serialVersionUID = 2L;

    public Project(@NotNull String name,
                   @Nullable String description,
                   @Nullable Date dateStart,
                   @Nullable Date dateFinish) throws ParseException {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        status = Status.PLANNED;
    }

    @NotNull
    @lombok.SneakyThrows
    @Override
    public String toString() {
        return "***\n[PROJECT]\nNAME: " + name + '\n' +
                "ID: " + id +
                "\n DESCRIPTION: " + description +
                "\n START DATE: " + DateParser.fromDateToString(dateStart) +
                "\n FINISH DATE: " + DateParser.fromDateToString(dateFinish);
    }
}

