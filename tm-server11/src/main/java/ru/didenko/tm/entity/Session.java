package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.constant.Role;

import java.io.Serializable;
import java.security.Signature;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Role role;

    @NotNull
    private String userId;

    @NotNull
    private Date creationDate = new Date();

    @NotNull
    private String signature;

    @NotNull
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
