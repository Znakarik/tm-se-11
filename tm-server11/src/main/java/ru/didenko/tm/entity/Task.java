package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Status;
import ru.didenko.tm.util.DateParser;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractGoal implements Serializable {
    @Nullable
    private String projectId;

    public Task(@NotNull String name, @Nullable String description, @Nullable Date dateStart, @Nullable Date dateFinish) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        status = Status.PLANNED;
    }

    @NotNull
    @Override
    public String toString() {
        return " TASK: " + '\n' +
                "STATUS: " + status + "\n" +
                "ID: " + id + '\n' +
                "NAME: " + name + '\n' +
                "DESCRIPTION: " + description + '\n' +
                "START DATE: " + DateParser.fromDateToString(dateStart) + '\n' +
                "FINISH DATE: " + DateParser.fromDateToString(dateFinish) + '\n' +
                "PROJECT ID: " + projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
