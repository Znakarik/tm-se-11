package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.util.HasherPassword;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

public final class User extends AbstractEntity implements Serializable {

    @NotNull
    String name;
    @Nullable
    private String password;
    @Nullable
    private Role role;

    public User() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User(@NotNull String name,
                @NotNull String password,
                @NotNull Role role) throws NoSuchAlgorithmException {
        this.name = name;
        this.password = HasherPassword.fromStringToHash(password);
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        return "[USER]" +
                "LOGIN:" + name + '\'' +
                ", ROLE: " + role +
                ", ID: " + id + '\'';
    }
}
