package ru.didenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.AbstractEntity;
import ru.didenko.tm.util.Assertion;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement
public abstract class AbstractRepository<T extends AbstractEntity> implements ru.didenko.tm.api.repository.IAbstractRepository<T> {
    @NotNull
    Map<String, T> entityMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<T> findAll() {
        return entityMap.values();
    }

    @Nullable
    @Override
    public T findOne(@NotNull String id) {
        if (!Assertion.assertNotNull(id)) return null;
        return entityMap.get(id);
    }

    @Nullable
    @Override
    public T persist(@NotNull T entity) {
        if (!Assertion.assertNotNull(entity)) return null;
        return entityMap.put(entity.getId(), entity);
    }

    @Nullable
    @Override
    public List<T> mergeAll(@Nullable List<T> entity) {
        for (T object : entity) {
            entityMap.put(object.getId(), object);
        }
        return new ArrayList<>(this.findAll());
    }


    @Override
    public void remove(@NotNull String id) {
        if (!Assertion.assertNotNull(id)) return;
        findAll().removeIf(t -> t.getId().equals(id));
    }

    @Override
    public void removeAll() {
        Iterator<T> iterator = findAll().iterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }
}
