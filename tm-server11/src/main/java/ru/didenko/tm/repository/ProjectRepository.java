package ru.didenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.project.IProjectRepository;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.util.Assertion;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project merge(@NotNull Project project) {
        Project updProject = findOne(project.getId());
        updProject.setName(project.getName());
        updProject.setDateStart(project.getDateStart());
        updProject.setDescription(project.getDescription());
        updProject.setDateFinish(project.getDateFinish());
        updProject.setId(updProject.getId());
        updProject.setUserId(project.getUserId());
        return persist(updProject);
    }

    @Override
    @Nullable
    public Project findOneWithUserId(@NotNull String userId, @NotNull String name) {
        if (!Assertion.assertNotNull(name, userId)) return null;
        @NotNull Project goal = secureFindOneByName(name, userId);
        if (!Assertion.assertNotNull(goal)) ;
        String goalUserId = goal.getUserId();
        assert goalUserId != null;
        if (goal.getUserId().equals(goalUserId)) {
            return goal;
        }
        return null;
    }

    public Project secureFindOneByName(String name, String userId) {
        if (!Assertion.assertNotNull(name, userId)) return null;
        Project project = null;
        List<Project> projects = findAllWithUserId(userId);
        if (!Assertion.assertNotNull(project)) return null;
        for (Project project1 : findAllWithUserId(userId)) {
            assert project1.getName() != null;
            if (project1.getName().equals(name)) project = project1;
        }
        return project;
    }

    @Nullable
    public List<Project> findAllWithUserId(@NotNull String userId) {
        List<Project> projects = new ArrayList<>();
        if (!Assertion.assertNotNull(userId)) return null;
        for (Project project : this.findAll()) {
            String id = project.getUserId();
            assert id != null;
            if (id.equals(userId)) {
                projects.add(project);
            }
        }
        return projects;
    }

}
