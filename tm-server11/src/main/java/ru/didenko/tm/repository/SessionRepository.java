package ru.didenko.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.session.ISessionRepository;
import ru.didenko.tm.entity.Session;

@Getter
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @Override
    public Session merge(Session session) {
        Session mergedSession = findOne(session.getId());
        mergedSession.setId(session.getId());
        mergedSession.setCreationDate(session.getCreationDate());
        mergedSession.setRole(session.getRole());
        mergedSession.setUserId(session.getUserId());
        return mergedSession;
    }

    @Override
    public Session findOne(@NotNull String userID, @NotNull String id) {
        for (@Nullable Session session : findAll()) {
            if (id.equals(session.getId()) && userID.equals(session.getUserId())) return session;
        }
        return null;
    }

    @Override
    public void remove(@NotNull String userID, @NotNull String id) {
        findAll().removeIf(session -> userID.equals(session.getId()) && id.equals(session.getId()));
    }
}
