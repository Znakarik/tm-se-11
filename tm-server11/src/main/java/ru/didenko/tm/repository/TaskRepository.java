package ru.didenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.util.Assertion;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public final class TaskRepository extends AbstractRepository<Task> implements ru.didenko.tm.api.task.ITaskRepository {

    @Nullable
    @Override
    public Task merge(@NotNull Task task) {
        Task updTask = findOne(task.getId());
        updTask.setName(task.getName());
        updTask.setUserId(task.getUserId());
        updTask.setProjectId(task.getProjectId());
        updTask.setDescription(task.getDescription());
        updTask.setDateStart(task.getDateStart());
        updTask.setDateFinish(task.getDateFinish());
        updTask.setId(task.getId());
        return persist(updTask);
    }

    @Override
    @Nullable
    public Task findOneWithUserId(@NotNull String userId, @NotNull String name) {
        if (!Assertion.assertNotNull(name, userId)) return null;
        @NotNull Task goal = secureFindOneByName(name, userId);
        if (!Assertion.assertNotNull(goal)) return null;
        String goalUserId = goal.getUserId();
        assert goalUserId != null;
        if (goal.getUserId().equals(goalUserId)) {
            return goal;
        }
        return null;
    }

    @Override
    public Task secureFindOneByName(String name, String userId) {
        if (!Assertion.assertNotNull(name, userId)) return null;
        Task task = null;
        List<Task> tasks = findAllWithUserId(userId);
        if (!Assertion.assertNotNull(tasks)) return null;
        for (Task task1 : findAllWithUserId(userId)) {
            assert task1.getName() != null;
            if (task1.getName().equals(name)) task = task1;
        }
        return task;
    }


    @Override
    @Nullable
    public List<Task> findAllWithUserId(@NotNull String userId) {
        List<Task> tasks = new ArrayList<>();
        if (!Assertion.assertNotNull(userId)) return null;
        for (Task task : this.findAll()) {
            String id = task.getId();
            assert id != null;
            if (id.equals(userId)) {
                tasks.add(task);
            }
        }
        return tasks;
    }

}
