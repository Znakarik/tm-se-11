package ru.didenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.user.IUserRepository;
import ru.didenko.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@XmlRootElement
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Nullable
    @Override
    public User merge(@NotNull User user) {
        User updUser = findOne(user.getId());
        Objects.requireNonNull(updUser).setName(user.getName());
        updUser.setId(updUser.getId());
        updUser.setRole(user.getRole());
        return persist(updUser);
    }
}
