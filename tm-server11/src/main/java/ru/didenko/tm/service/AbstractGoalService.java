package ru.didenko.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.IGoalService;
import ru.didenko.tm.constant.Status;
import ru.didenko.tm.entity.AbstractGoal;
import ru.didenko.tm.repository.AbstractRepository;
import ru.didenko.tm.util.Assertion;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractGoalService<T extends AbstractGoal> extends AbstractService<T> implements IGoalService<T> {

    public AbstractGoalService(@NotNull final AbstractRepository<T> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    @Nullable
    public final T search(@NotNull final String searchVal) {
        if (!Assertion.assertNotNull(searchVal)) return null;
        for (T entity : findAll()) {
            assert entity.getDescription() != null;
            assert entity.getName() != null;
            if (entity.getName().contains(searchVal) || entity.getDescription().contains(searchVal)) {
                return entity;
            }
        }
        return null;
    }

    @Override
    public final List<T> getList() {
        return new ArrayList<T>(findAll());
    }

    @Override
    @NotNull
    public final void sortByStart(List<T> list) {
        list.sort(Comparator.comparing(T::getDateStart));
    }

    @Override
    @NotNull
    public final void sortByCreation(List<T> list) {
        list.sort(Comparator.comparing(T::getCreation));
    }

    @Override
    @NotNull
    public final void sortByFinish(List<T> list) {
        list.sort(Comparator.comparing(T::getDateFinish));
    }

    @Override
    @NotNull
    public final void sortByStatus(List<T> list) {
        final Integer[] i = {null};
        final Comparator<T> statusComparator = (o1, o2) -> {
            if (o1.getStatus().equals(o2.getStatus())) i[0] = 0;
            else if (o1.getStatus().equals(Status.PLANNED)) i[0] = -1;
            else if (o2.getStatus().equals(Status.PLANNED)) i[0] = 1;
            else if (o1.getStatus().equals(Status.IN_PROCESS)) i[0] = -1;
            else if (o2.getStatus().equals(Status.IN_PROCESS)) i[0] = 1;
            return i[0];
        };
        list.sort(statusComparator);
    }

    @Override
    public final void editDescription(final @NotNull T entity, final @NotNull String newDescription) {
        if (!Assertion.assertNotNull(entity, newDescription)) return;
        entity.setDescription(newDescription);
    }

    @Override
    @Nullable
    public T findOneByName(@NotNull String name) {
        if (!Assertion.assertNotNull(name)) return null;
        T goal = null;
        for (T t : findAll()) {
            if (t.getName().equals(name)) return goal = t;
        }
        return goal;
    }
}
