package ru.didenko.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.IAbstractService;
import ru.didenko.tm.entity.AbstractEntity;
import ru.didenko.tm.repository.AbstractRepository;
import ru.didenko.tm.util.Assertion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IAbstractService<T> {
    @Nullable
    AbstractRepository<T> abstractRepository;

    public AbstractService(@Nullable final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public Collection<T> findAll() {
        return abstractRepository.findAll();
    }

    @Override
    @Nullable
    public List<T> findList() {
        return new ArrayList<>(abstractRepository.findAll());
    }

    @Override
    @Nullable
    public List<T> addAll(@Nullable List<T> entity) {
        return abstractRepository.mergeAll(entity);
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }

    @Override
    public void showAll() {
        System.out.println(abstractRepository.findAll().toString());
    }
//
//    @Nullable
//    public T findOneByName(@NotNull String name) {
//        if (!Assertion.assertNotNull(name)) return null;
//        return abstractRepository.findOneByName(name);
//    }

    @Override
    public void delete(@NotNull String id) {
        if (!Assertion.assertNotNull(id)) return;
        abstractRepository.remove(id);
    }

    @Override
    public T persist(T entity) {
        if (!Assertion.assertNotNull(entity)) return null;
        return abstractRepository.persist(entity);
    }
}
