package ru.didenko.tm.service;


import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.project.IProjectService;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.repository.ProjectRepository;
import ru.didenko.tm.util.Assertion;
import ru.didenko.tm.util.DateParser;

import java.text.ParseException;
import java.util.List;

@Getter
public final class ProjectService extends AbstractGoalService<Project> implements IProjectService {
    @NotNull
    final private ProjectRepository projectRepository = (ProjectRepository) abstractRepository;

    public ProjectService(@NotNull final ProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    public final void createNew(@NotNull final String name,
                                final @Nullable String description,
                                final @Nullable String start,
                                final @Nullable String finish) throws ParseException {
        if (!Assertion.assertNotNull(name)) return;
        abstractRepository.persist(new Project(name, description, DateParser.fromStringToDate(start), DateParser.fromStringToDate(finish)));
    }


    @Nullable
    @Override
    public final Project secureGetOne(@NotNull String userId, @NotNull String name) {
        if (!Assertion.assertNotNull(userId, name)) return null;
        return projectRepository.findOneWithUserId(userId, name);
    }

    @Override
    @Nullable
    public final List<Project> getSecureList(@NotNull String userId) {
        if (!Assertion.assertNotNull(userId)) return null;
        return projectRepository.findAllWithUserId(userId);
    }

}