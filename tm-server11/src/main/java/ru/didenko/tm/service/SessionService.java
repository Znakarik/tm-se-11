package ru.didenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.session.ISessionRepository;
import ru.didenko.tm.api.session.ISessionService;
import ru.didenko.tm.entity.Session;
import ru.didenko.tm.repository.AbstractRepository;
import ru.didenko.tm.util.Assertion;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository repository = (ISessionRepository) abstractRepository;

    public SessionService(@NotNull final AbstractRepository<Session> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    public List<Session> getList() {
        return (List<Session>) repository.findAll();
    }

    @Override
    public void remove(@NotNull final String id, @NotNull final String userId) {
        if (Assertion.assertNotNull(id, userId)) return;
        repository.remove(id, userId);
    }

    @Override
    @Nullable
    public Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (!Assertion.assertNotNull(userId, id)) return null;
        return repository.findOne(userId, id);
    }
}
