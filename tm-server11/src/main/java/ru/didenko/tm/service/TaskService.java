package ru.didenko.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.task.ITaskService;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.repository.TaskRepository;
import ru.didenko.tm.util.Assertion;
import ru.didenko.tm.util.DateParser;

import java.text.ParseException;
import java.util.List;

@Getter
public final class TaskService extends AbstractGoalService<Task> implements ITaskService {
    @NotNull
    private final TaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(final @NotNull TaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public final void createNew(final @NotNull String name,
                                final @Nullable String description,
                                final @Nullable String start,
                                final @Nullable String finish) throws ParseException {
        if (!Assertion.assertNotNull(name)) return;
        final Task task = new Task(name, description, DateParser.fromStringToDate(start), DateParser.fromStringToDate(finish));
        abstractRepository.persist(task);
    }

    @Override
    public final void setProject(final @NotNull String taskName, final @NotNull String projectId) {
        if (!Assertion.assertNotNull(taskName, projectId)) return;
        findOneByName(taskName).setProjectId(projectId);
    }

    @Nullable
    public final Task secureGetOne(@NotNull String userId, @NotNull String name) {
        if (Assertion.assertNotNull(userId, name)) return null;
        return taskRepository.findOneWithUserId(userId, name);
    }

    @Override
    public final List<Task> getSecureList(@NotNull String userId) {
        if (Assertion.assertNotNull(userId)) return null;
        return taskRepository.findAllWithUserId(userId);
    }
}