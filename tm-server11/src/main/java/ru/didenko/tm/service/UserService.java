package ru.didenko.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.project.IProjectService;
import ru.didenko.tm.api.task.ITaskService;
import ru.didenko.tm.api.user.IUserService;
import ru.didenko.tm.constant.AppConstants;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.dto.Domain;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.repository.AbstractRepository;
import ru.didenko.tm.repository.UserRepository;
import ru.didenko.tm.util.Assertion;
import ru.didenko.tm.util.HasherPassword;
import ru.didenko.tm.util.data.SignatureUtil;
import ru.didenko.tm.util.data.load.DataImporter;
import ru.didenko.tm.util.data.save.DataExporter;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static ru.didenko.tm.constant.AppConstants.*;
import static ru.didenko.tm.constant.AppConstants.SALT;

@Getter
@XmlRootElement
public final class UserService extends AbstractService<User> implements IUserService {
    @XmlElement(name = "user")
    @NotNull
    private final UserRepository userRepository = (UserRepository) abstractRepository;
    @NotNull
    final ITaskService taskService;
    @NotNull
    final IProjectService projectService;
    @Nullable
    private User currentUser;

    public UserService(final @NotNull AbstractRepository<User> userRepository,
                       @NotNull IProjectService projectService,
                       @NotNull ITaskService taskService) {
        super(userRepository);
        this.projectService = projectService;
        this.taskService = taskService;
    }
//
//    @Override
//    public User getCurrentUser() {
//        return currentUser;
//    }
//
//    @Override
//    public void setCurrentUser(@Nullable User currentUser) {
//        this.currentUser = currentUser;
//    }

    @Override
    public User createUser(final @NotNull String login, final @NotNull String pass, final @NotNull Role role) {
        @NotNull User user = new User();
        user.setName(login);
        user.setPassword(SignatureUtil.sign(pass, SALT, CYCLE));
        user.setRole(role);
        userRepository.persist(user);
        return user;
    }

    @Override
    public User loadUser(final @NotNull String login, final @NotNull String pass, final @NotNull Role role) {
        @NotNull User user = new User();
        user.setName(login);
        user.setPassword(pass);
        user.setRole(role);
        userRepository.persist(user);
        return user;
    }

    @Override
    public void loadUsers(@Nullable final List<User> users) {
        if (users == null) return;
        for (User user : users) {
            loadUser(user.getName(), user.getPassword(), user.getRole());
        }
    }

    @Override
    public void setCurrentUserByName(final @NotNull String name) {
        currentUser = findOneByName(name);
    }

    @Nullable
    @Override
    public User findUser(final @NotNull String login, final @NotNull String pass) throws NoSuchAlgorithmException {
        User user = findOneByName(login);
        String userPass = user != null ? user.getPassword() : null;
        assert userPass != null;
        if (HasherPassword.checkPasswords(userPass, pass)) {
            return findOneByName(login);
        }
        return null;
    }

    @Override
    public List<User> getList() {
        return new ArrayList<>(findAll());
    }

    @Override
    @Nullable
    public User findOneByName(@NotNull String name) {
        if (!Assertion.assertNotNull(name)) return null;
        User requiredUser = null;
        for (User user1 : findAll()) {
            if (user1.getName().equals(name)) requiredUser = user1;
        }
        return requiredUser;
    }

    @Override
    public void binSave() throws IOException {
        @NotNull final Domain domain = new Domain();
        domain.save(projectService, taskService, this);
        DataExporter.binSave(domain);
    }

    @Override
    public void jsonJacksonSave() throws IOException {
        @NotNull final Domain domain = new Domain();
        domain.save(projectService, taskService, this);
        DataExporter.jsonJacksonSave(domain);
    }

    @Override
    public void xmlJacksonSave() throws IOException {
        @NotNull final Domain domain = new Domain();
        domain.save(projectService, taskService, this);
        DataExporter.xmlJacksonSave(domain);
    }

    @Override
    public void jsonJaxbSave() throws JAXBException {
        @NotNull final Domain domain = new Domain();
        domain.save(projectService, taskService, this);
        DataExporter.jsonJaxbSave(domain);
    }

    @Override
    public void xmlJaxbSave() throws JAXBException {
        @NotNull final Domain domain = new Domain();
        domain.save(projectService, taskService, this);
        DataExporter.xmlJaxbSave(domain);
    }

    @Override
    public void binLoad() throws IOException, ClassNotFoundException {
        @NotNull final Domain domain = DataImporter.binLoad();
        domain.load(projectService, taskService, this);
    }

    @Override
    public void jsonJacksonLoad() throws IOException {
        @NotNull final Domain domain = DataImporter.jsonJacksonLoad();
        domain.load(projectService, taskService, this);
    }

    @Override
    public User persist(User entity) {
        return super.persist(entity);
    }

    @Override
    public void jsonJaxbLoad() throws JAXBException {
        @NotNull final Domain domain = DataImporter.jsonJaxbLoad();
        domain.load(projectService, taskService, this);
    }

    @Override
    public void xmlJacksonLoad() throws IOException {
        @NotNull final Domain domain = DataImporter.xmlJacksonLoad();
        domain.load(projectService, taskService, this);
    }

    @Override
    public void xmlJaxbLoad() throws IOException, JAXBException {
        @NotNull final Domain domain = DataImporter.xmlJaxbLoad();
        domain.load(projectService, taskService, this);
    }
}
