package ru.didenko.tm.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateParser {

    public final static DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    public static Date fromStringToDate(String date) throws ParseException {
        Date date1 = null;
        if (date != null && !date.equals("")) {
            date1 = DATE_FORMAT.parse(date);
        }
        return date1;
    }

    public static String fromDateToString(Date date) {
        return DATE_FORMAT.format(date);
    }
}
