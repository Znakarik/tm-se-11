package ru.didenko.tm.util;

import org.jetbrains.annotations.Nullable;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static java.security.MessageDigest.getInstance;

public final class HasherPassword {
    static MessageDigest md;

    public HasherPassword() throws NoSuchAlgorithmException {
    }

    public static String fromStringToHash(String password) throws NoSuchAlgorithmException {
        md = getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    public static boolean checkPasswords(String first, String second) throws NoSuchAlgorithmException {
        return fromStringToHash(first).equals(fromStringToHash(second));
    }

    public static void main(String[] args) {
        System.out.println(md5("1"));
    }

    public static String md5(@Nullable final String md5) {
        if (md5 == null) return null;
        try {
            md = getInstance("MD5");
            byte[] arr = md.digest(md5.getBytes());
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < arr.length; ++i) {
                stringBuilder.append(Integer.toHexString((arr[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
