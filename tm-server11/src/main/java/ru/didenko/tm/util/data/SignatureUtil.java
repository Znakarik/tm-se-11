package ru.didenko.tm.util.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.util.HasherPassword;

public final class SignatureUtil {

    @Nullable
    public static String sign(
            @Nullable final Object val,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(val);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String sign(
            @Nullable final String val,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        if (val == null || salt == null || cycle == null) return null;
        String result = val;
        for (int i = 0; i < cycle; i++) {
            result = HasherPassword.md5(salt + result + salt);
        }
        return result;
    }
}
