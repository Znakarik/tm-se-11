package ru.didenko.tm.util.data.load;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;

import javax.xml.bind.JAXBContext;

import org.eclipse.persistence.jaxb.JAXBContextFactory;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class DataImporter {

    public static Domain binLoad() throws IOException, ClassNotFoundException {
        @NotNull final File file = new File(DataFiles.BIN_DATA_FILE.getFilePath());
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
        return (Domain) objectInputStream.readObject();
    }

    public static Domain jsonJacksonLoad() throws IOException {
        @NotNull final File file = new File(DataFiles.JACKSON_JSON_DATA.getFilePath());
        @NotNull final JsonMapper mapper = new JsonMapper();
        return (Domain) mapper.readValue(file, Domain.class);
    }

    public static Domain jsonJaxbLoad() throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File(DataFiles.JSON_DATA_FILE.getFilePath());
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty("eclipselink.media-type", "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        return (Domain) unmarshaller.unmarshal(file);
    }

    public static Domain xmlJacksonLoad() throws IOException {
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final File file = new File(DataFiles.JACKSON_XML_DATA.getFilePath());
        return (Domain) mapper.readValue(file, Domain.class);
    }

    public static Domain xmlJaxbLoad() throws FileNotFoundException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File(DataFiles.XML_JAXB_DATA.getFilePath());
        @NotNull final FileReader reader = new FileReader(file);
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        return (Domain) unmarshaller.unmarshal(reader);
    }
}
