package ru.didenko.tm.util.data.save;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.constant.DataFiles;
import ru.didenko.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.*;

public class DataExporter {

    public static void binSave(@NotNull final Domain domain) throws IOException {
        @NotNull final FileOutputStream outputStream = new FileOutputStream(DataFiles.BIN_DATA_FILE.getFilePath());
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    public static void jsonJacksonSave(@NotNull final Domain domain) throws IOException {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File file = new File(DataFiles.JACKSON_JSON_DATA.getFilePath());
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    }

    public static void xmlJacksonSave(@NotNull final Domain domain) throws IOException {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final File file = new File(DataFiles.JACKSON_XML_DATA.getFilePath());
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    }

    public static void jsonJaxbSave(@NotNull final Domain domain) throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty("eclipselink.media-type", "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final File file = new File(DataFiles.JSON_DATA_FILE.getFilePath());
        marshaller.marshal(domain, file);
    }

    public static void xmlJaxbSave(@NotNull final Domain domain) throws JAXBException {
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, new File(DataFiles.XML_JAXB_DATA.getFilePath()));
    }
}
